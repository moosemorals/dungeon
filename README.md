# Dungeon - Another Roguelike

Based on the (fantastic!) [Trystan's tutorial](https://trystans.blogspot.com/2016/01/roguelike-tutorial-00-table-of-contents.html?m=1),
a simple rougelike written in C#.

Medium term goals include:
  - Add a web front end, why not.

    This brings in a bunch of questions about multiplayer blah blah, but mostly
    I just want to be able to play at work.

  - More AI

    Really, that should be better or smarter AI, but who am I kidding?

    There's all that steering behaviours stuff, hierarchical state machines,
    subsumption architecture, the full works. Which blends into:

 - Permanant world

   Rather than generate a world on every new start, create a world with
   enough going on that I can leave it running in the background (on, say,
   a webserver somewhere) and come back later to find that the Goblins
   and the Dwarfs have allied against the Elves in the local baking
   competition.

However, I've got a fair way to go to even finish the tutorial, let's
see if I can maintain interest that far!

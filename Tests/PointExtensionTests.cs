// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.tests;

using uk.osric.dungeon.world;

public class PointExtensionTests {

    public static readonly object[] GetFacingTestCases = new object[] {
        new object[] { new Point(1, 0), Facing.Right },
        new object[] { new Point(-1, 0), Facing.Left },
        new object[] { new Point(0, 1), Facing.Down },
        new object[] { new Point(0, -1), Facing.Up },
    };

    [TestCaseSource(nameof(GetFacingTestCases))]
    public void GetFacingTests(Point p, Facing expected) => Assert.That(p.GetFacing(), Is.EqualTo(expected));
}

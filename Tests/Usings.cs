// SPDX-Licence-Idenitfier: ISC

#pragma warning disable IDE0065 // Misplaced using directive

global using NUnit.Framework;

#pragma warning restore IDE0065 // Misplaced using directive

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.tests;

using uk.osric.dungeon.library;

public class SchedulerTests {

    [TestCaseSource(nameof(SimpleScheduleTestCases))]
    public void SimpleScheduleTest(int[] items, double[] priorities, int[] expected) {
        Scheduler<int> scheduler = new();
        for (int i = 0; i < items.Length; i += 1) {
            scheduler.Schedule(items[i], priorities[i]);
        }

        List<int> actual = new();
        while (scheduler.HasNext) {
            actual.Add(scheduler.GetNext());
        }

        Assert.That(actual, Is.EquivalentTo(expected));
    }

    private static readonly object[] SimpleScheduleTestCases = new object[] {
        new object[] { new int[] { 1, 2, 3 }, new double[] { 1, 2, 3 }, new int[] { 1, 2, 3 } },
        new object[] { new int[] { 3, 2, 1 }, new double[] { 1, 2, 3 }, new int[] { 3, 2, 1 } }
    };
}

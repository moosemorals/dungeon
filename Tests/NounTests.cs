﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.tests;

using uk.osric.dungeon.library;

internal class NounTests {

    [TestCaseSource(nameof(ArticleTestCases))]
    public void ArticleTests(Noun noun, string expected) {
        Assert.That(noun.ToString(), Is.EqualTo(expected));
    }

    public static readonly object[] ArticleTestCases = new object[] {
        new object[] { new Noun("cat"), "a cat" },
        new object[] { new Noun("cat").WithCount(2), "2 cats" },
        new object[] { new Noun("cat").AsPlayer, "the cat" },
        new object[] { new Noun("cat").AsPlayer.WithCount(2), "2 cats" },

        new object[] { new Noun("egg"), "an egg" },
        new object[] { new Noun("egg").WithCount(2), "2 eggs" },
        new object[] { new Noun("egg").AsPlayer, "the egg" },
        new object[] { new Noun("egg").WithCount(2).AsPlayer, "2 eggs" },
    };
}

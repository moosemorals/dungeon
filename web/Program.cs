namespace uk.osric.dungeon.web;

public class Program {

    public static void Main(string[] args) {
        WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

        builder.Services.AddControllers();

        WebApplication app = builder.Build();

        DefaultFilesOptions options = new();
        options.DefaultFileNames.Clear();
        options.DefaultFileNames.Add("index.html");

        app.UseWebSockets();

        app.UseDefaultFiles();
        app.UseStaticFiles();

        app.MapControllers();

        app.Run();
    }
}

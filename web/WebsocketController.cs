﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.web;

using System.Net.WebSockets;

using Microsoft.AspNetCore.Mvc;

[Route("")]
public class WebsocketController : ControllerBase {

    [HttpGet("/ws")]
    public async Task WebsocketAsync() {
        if (HttpContext.WebSockets.IsWebSocketRequest) {
            using WebSocket webSocket = await HttpContext.WebSockets.AcceptWebSocketAsync();
            WebSocketDisplay display = new(webSocket);

            DungeonEngine engine = new(display);
            await engine.RunAsync();
        } else {
            HttpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        }
    }
}

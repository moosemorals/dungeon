﻿import { $, $$, tag } from './lib.js';

class Terminal {
    constructor(cols, rows) {
        this.rows = rows;
        this.cols = cols;

        this.row = 1;
        this.col = 1;

        this.genStyles(cols, rows);

        this.term = $("#terminal");

        this.color = "";

        this.commands = {
            'J': this.clearScreen,
            'X': this.removeChars,
            'H': this.setCursorPos,
            'm': this.setColor
        }
    }

    connect() {
        const loc = document.location;
        const target = `${loc.protocol == 'https' ? 'wss' : 'ws'}://${loc.host}/ws`;

        this.ws = new WebSocket(target);

        this.ws.onopen = e => console.log(e);
        this.ws.onerror = e => console.log(e);
        this.ws.onmessage = e => this.drawScreen(e.data);

        document.addEventListener("keydown", k => {
            this.ws.send(k.key);
        });
    }

    genStyles(cols, rows) {
        const style = tag("style");
        document.head.appendChild(style);
        const css = style.sheet;
        for (let c = 1; c <= cols; c += 1) {
            css.insertRule(`#terminal .col${c}{grid-column:${c};}`, css.cssRules.length);
        }

        for (let r = 1; r < rows; r += 1) {
            css.insertRule(`#terminal .row${r}{grid-row:${r};}`, css.cssRules.length);
        }

        // Colors
        const hues = [0, 120, 60, 240, 320, 180];
        for (let i = 0; i < hues.length; i += 1) {
            const hue = hues[i];
            css.insertRule(`#terminal .m${31 + i} {color:hsl(${hue},var(--normal-saturation),50%);}`, css.cssRules.length);
            css.insertRule(`#terminal .m${41 + i} {background-color:hsl(${hue},var(--normal-saturation),50%);}`, css.cssRules.length);
            css.insertRule(`#terminal .m${91 + i} {color:hsl(${hue},var(--bright-saturation),50%);}`, css.cssRules.length);
            css.insertRule(`#terminal .m${101 + i} {background-color:hsl(${hue},var(--bright-saturation),50%);}`, css.cssRules.length);
        }

        // Black and white
        const lums = [0, 100];
        for (let i = 0; i < lums.length; i += 1) {
            const lum = lums[i];
            const m = i * 7;
            css.insertRule(`#terminal .m${30 + m} {color:hsl(0,var(--normal-saturation),${lum}%);}`, css.cssRules.length);
            css.insertRule(`#terminal .m${40 + m} {background-color:hsl(0,var(--normal-saturation),${lum}%);}`, css.cssRules.length);
            css.insertRule(`#terminal .m${90 + m} {color:hsl(0,var(--bright-saturation),${lum}%);}`, css.cssRules.length);
            css.insertRule(`#terminal .m${100 + m} {background-color:hsl(0,var(--bright-saturation),${lum}%);}`, css.cssRules.length);
        }
    }

    setColor(...colors) {
        this.color = colors.map(c => `m${c}`)
            .join(" ");
    }

    clearScreen(mode) {
        if (mode == 2) {
            $$(this.term, "div").forEach(d => this.term.removeChild(d));
        } else {
            console.error(`Clear Screen: Mode ${mode} not supported`);
        }
    }

    removeChars(count) {
        for (let c = 0; c < count; c += 1) {
            this.removeChar(this.col + c, this.row);
        }
    }

    // Notice that ANSI gives us the cords the wrong way round.
    setCursorPos(row, col) {
        this.col = col;
        this.row = row;
    }

    removeChar(col, row) {
        const div = $(`.col${col}.row${row}`);
        if (div != null) {
            this.term.removeChild(div);
        }
    }

    writeChar(col, row, c) {
        this.removeChar(col, row);

        if (c == " ") {
            return;
        }

        const clazz = [`col${col}`, `row${row}`, this.color]
            .filter(x => x != null && x != "")
            .join(' ');

        const div = tag("div", clazz, c[0]);
        this.term.appendChild(div);
    }

    drawScreen(string) {
        function isAtEnd() { return current >= string.length; }
        function advance() { return string[current++]; }
        function peek() { return string[current]; }
        function match(expected) {
            if (isAtEnd() || peek() != expected) {
                return false;
            }
            advance();
            return true;
        }

        function parseEscape() {
            const seq = {
                args: [],
                cmd: ""
            }

            if (!match('[')) {
                return seq;
            }

            let c = peek();
            if (c >= '0' && c <= '9') {
                // parameters
                let digits = advance();
                while (true) {
                    c = peek();
                    if (c >= '0' && c <= '9') {
                        digits += advance();
                    } else if (c == ';') {
                        seq.args.push(parseInt(digits, 10));
                        digits = "";
                        advance();
                    } else {
                        if (digits.length > 0) {
                            seq.args.push(parseInt(digits, 10));
                        }
                        break;
                    }
                }
            }

            seq.cmd = advance();

            return seq;
        }

        let current = 0; // Current position in input

        while (!isAtEnd()) {
            const c = advance();
            if (c == '\x1B') {
                const seq = parseEscape();
                if (seq.cmd in this.commands) {
                    this.commands[seq.cmd].apply(this, seq.args);
                }
            } else {
                this.writeChar(this.col++, this.row, c);
            }
        }
    }
}

window.addEventListener("DOMContentLoaded", () => {
    new Terminal(83, 25).connect();
});

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.web;

using System.Net.WebSockets;
using System.Text;
using System.Threading;

using uk.osric.dungeon.display;
using uk.osric.dungeon.library;

public partial class WebSocketDisplay : IDisplay {

    public WebSocketDisplay(WebSocket webSocket) {
        _writeBuffer = new();
        _webSocket = webSocket;
    }

    public void Dispose() => GC.SuppressFinalize(this);

    public async Task<uint> FlushAsync() {
        uint count = (uint)_writeBuffer.Count;
        ArraySegment<byte> messageBytes = new(_writeBuffer.ToArray());
        await _webSocket.SendAsync(messageBytes, WebSocketMessageType.Text, true, default);

        _writeBuffer.Clear();
        return count;
    }

    public (int row, int col) GetCursorPos() => (0, 0);

    public (int rows, int cols) GetSize() => (80, 25);

    public async Task<int> ReadKeyAsync(CancellationToken token = default) {
        WebSocketReceiveResult receiveResult;
        byte[] buff = new byte[1024];

        byte[] messageBytes = Array.Empty<byte>();
        do {
            receiveResult = await _webSocket.ReceiveAsync(new ArraySegment<byte>(buff), token);
            if (receiveResult.CloseStatus != null) {
                break;
            }

            byte[] scratch = new byte[messageBytes.Length + receiveResult.Count];
            Array.Copy(messageBytes, scratch, messageBytes.Length);
            Array.Copy(buff, 0, scratch, messageBytes.Length, receiveResult.Count);
            messageBytes = scratch;
        } while (!receiveResult.EndOfMessage);

        string messageString = Encoding.UTF8.GetString(messageBytes);
        if (!string.IsNullOrEmpty(messageString)) {
            if (messageString.Length == 1) {
                return messageString[0];
            } else {
                return messageString switch {
                    "Escape" => '\x1B',
                    "Enter" => '\r',
                    "ArrowLeft" => (int)KeyCodes.ArrowLeft,
                    "ArrowRight" => (int)KeyCodes.ArrowRight,
                    "ArrowUp" => (int)KeyCodes.ArrowUp,
                    "ArrowDown" => (int)KeyCodes.ArrowDown,
                    _ => '\0',
                };
            }
        }

        return 0;
    }

    public void Reset() => throw new NotImplementedException();

    public void Write(byte[] bytes, int offset, int length) {
        byte[] scratch = new byte[length];
        Array.Copy(bytes, offset, scratch, 0, length);
        _writeBuffer.AddRange(scratch);
    }

    private readonly WebSocket _webSocket;

    private readonly List<byte> _writeBuffer;
}

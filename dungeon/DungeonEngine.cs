﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon;

using System.Threading.Tasks;

using uk.osric.dungeon.display;
using uk.osric.dungeon.states;

public class DungeonEngine {

    public DungeonEngine(IDisplay display) {
        _display = display;
        _gameState = new StartState();
    }

    public async Task RunAsync() {
        _display.Write(Ansi.HideCursor());
        await RepaintAsync();
        bool running = true;
        while (running) {
            int key = await _display.ReadKeyAsync();
            if (key != '\0') {
                running = await OnKeyPressedAsync(key);
            }
        }
    }

    private readonly IDisplay _display;

    private IGameState _gameState;

    private async Task<bool> OnKeyPressedAsync(int key) {
        IGameState? nextState = _gameState.OnUserInput(key);
        if (nextState == null) {
            await QuitAsync();
            return false;
        }

        _gameState = nextState;
        await RepaintAsync();
        return true;
    }

    private async Task QuitAsync() {
        _display.Clear();
        _display.SetCursorPos(0, 0);
        _display.Reset();
        await _display.FlushAsync();
        Console.WriteLine();
        Console.WriteLine("Goodbye!");
    }

    private async Task RepaintAsync() {
        _display.Clear();
        _gameState.DrawOutput(_display);
        await _display.FlushAsync();
    }
}

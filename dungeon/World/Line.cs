﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using System.Collections;

internal class Line : IEnumerable<Point> {

    public IEnumerator<Point> GetEnumerator() => ((IEnumerable<Point>)_points).GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)_points).GetEnumerator();

    internal Line(Point from, Point to) : this(from.X, from.Y, to.X, to.Y) { }

    internal Line(int x0, int y0, int x1, int y1) {
        int signX = x0 < x1 ? 1 : -1;
        int signY = y0 < y1 ? 1 : -1;

        int dx = (x1 - x0) * signX;
        int dy = (y1 - y0) * signY;

        int err = dx - dy;

        while (true) {
            _points.Add(new Point(x0, y0));

            if (x0 == x1 && y0 == y1) {
                break;
            }

            int e2 = err * 2;
            if (e2 > -dx) {
                err -= dy;
                x0 += signX;
            }

            if (e2 < dx) {
                err += dx;
                y0 += signY;
            }
        }
    }

    private readonly List<Point> _points = new();
}

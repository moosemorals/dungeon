﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

internal interface ILocatable {

    internal int X { get; set; }

    internal int Y { get; set; }
}

internal static class ILocatableExtentions {

    internal static bool Inside(this ILocatable locatable, int left, int top, int width, int height) {
        return locatable.X >= left && locatable.X < left + width
            && locatable.Y >= top && locatable.Y < top + height;
    }
}

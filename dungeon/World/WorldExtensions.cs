﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using uk.osric.dungeon.actors;

internal static class WorldExtensions {

    internal static World Populate(this World world, Actor player) {
        for (int i = 0; i < 8; i += 1) {
            world.AddAtEmptyLocation(world.ActorFactory.Fungus());
        }

        for (int i = 0; i < 20; i += 1) {
            world.AddAtEmptyLocation(world.ActorFactory.Bat());
        }

        for (int i = 0; i < 4; i += 1) {
            world.AddAtEmptyLocation(world.ActorFactory.Zombie(player));
        }

        for (int i = 0; i < 4; i += 1) {
            world.AddAtEmptyLocation(world.ActorFactory.Goblin(player));
        }

        for (int i = 0; i < world.Width * world.Height / 20; i += 1) {
            world.AddAtEmptyLocation(ItemFactory.Rock());
        }

        for (int i = 0; i < world.Width * world.Height / 90; i += 1) {
            world.AddAtEmptyLocation(ItemFactory.TravelRation);
        }

        for (int i = 0; i < 9; i += 1) {
            world.AddAtEmptyLocation(ItemFactory.RandomWeapon);
            world.AddAtEmptyLocation(ItemFactory.RandomArmor);
            world.AddAtEmptyLocation(ItemFactory.RandomPotion);
        }

        return world;
    }
}

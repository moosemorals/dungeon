﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.display;
using uk.osric.dungeon.library;

internal record Tile(char Glyph, Style Style, Noun Name) : IDrawable {
    internal bool IsDiggable => this == Tiles.Wall;

    internal bool IsGround => this != Tiles.Wall && this != Tiles.Bounds;

    internal bool IsWalkable => this == Tiles.Floor;
}

internal static class Tiles {

    internal static Tile Bounds = new('x', new(AnsiColor.ForegroundBlack), new ("Out of bounds"));

    internal static Tile Floor = new('\u2591', new(AnsiColor.ForegroundWhite),new ( "Floor"));

    internal static Tile Unknown = new(' ', new(AnsiColor.ForegroundBlack),new ( "Unkown"));

    internal static Tile Wall = new('\u2588', new(AnsiColor.ForegroundWhite),new( "Wall"));
}

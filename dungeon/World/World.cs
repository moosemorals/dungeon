﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using System.Collections.Generic;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.library;

internal class World {

    internal World(SquareArray<Tile> tiles, int width, int height) {
        ActorFactory = new(this);
        _tiles = tiles;
        _items = new(width, height, null);
        Width = width;
        Height = height;
    }

    internal ActorFactory ActorFactory { get; private init; }

    internal IReadOnlyList<Actor> Actors => _actors.AsReadOnly();

    internal int Height { get; private init; }

    internal int Width { get; private init; }

    internal void AddAtEmptyLocation(Actor actor) {
        int worldX, worldY;
        do {
            worldX = Random.Shared.Next(Width);
            worldY = Random.Shared.Next(Height);
        } while (!CanEnter(worldX, worldY));

        actor.X = worldX;
        actor.Y = worldY;
        _actors.Add(actor);
    }

    internal void AddAtEmptyLocation(Item item) {
        int worldX, worldY;
        do {
            worldX = Random.Shared.Next(Width);
            worldY = Random.Shared.Next(Height);
        } while (!_tiles[worldX, worldY].IsGround || _items[worldX, worldY] != null);

        _items[worldX, worldY] = item;
    }

    internal bool AddItem(Item item, int worldX, int worldY) {
        Queue<Point> points = new();
        HashSet<Point> seen = new();

        points.Enqueue(new(worldX, worldY));

        while (points.Any()) {
            Point next = points.Dequeue();
            seen.Add(next);

            if (!_tiles[next.X, next.Y].IsGround) {
                continue;
            }

            if (_items[next.X, next.Y] == null) {
                _items[next.X, next.Y] = item;
                if (GetActor(next.X, next.Y) is Actor actor) {
                    actor.Notify($"A {item.Name} lands at your feet.");
                }

                return true;
            } else {
                foreach (Point p in next.Neighbors8().Except(seen)) {
                    points.Enqueue(p);
                }
            }
        }

        return false;
    }

    internal bool CanEnter(int worldX, int worldY)
            => GetTile(worldX, worldY).IsGround && GetActor(worldX, worldY) == null;

    internal void Dig(int worldX, int worldY) {
        if (GetTile(worldX, worldY).IsDiggable) {
            SetTile(worldX, worldY, Tiles.Floor);
        }
    }

    internal Actor? GetActor(int worldX, int worldY)
        => _actors.SingleOrDefault(a => a.X == worldX && a.Y == worldY);

    internal IDrawable GetDrawable(int worldX, int worldY) {
        if (_items.TryGet(worldX, worldY, out Item? maybeItem) && maybeItem is Item item) {
            return item;
        }

        return _tiles[worldX, worldY];
    }

    internal Item? GetItem(int worldX, int worldY) {
        if (_items.TryGet(worldX, worldY, out Item? result)) {
            return result;
        }

        return default;
    }

    internal Tile GetTile(int worldX, int worldY) {
        if (_tiles.TryGet(worldX, worldY, out Tile? tile)) {
            return tile ?? throw new NullReferenceException("Tiles can't be null");
        }

        return Tiles.Bounds;
    }

    internal void Remove(Actor actor) => _actors.Remove(actor);

    internal void Remove(Item item) {
        if (_items.Find(item) is Point p) {
            RemoveItemAt(p.X, p.Y);
        }
    }

    internal void RemoveItemAt(int worldX, int worldY)
        => _items[worldX, worldY] = null;

    internal void SetTile(int worldX, int worldY, Tile tile) {
        if (worldX >= 0 && worldX < Width && worldY >= 0 && worldY < Height) {
            _tiles[worldX, worldY] = tile;
        }
    }

    internal void Update() {
        // Take a copy of the list so we don't get
        // concurrent modification errors
        foreach (Actor actor in _actors.ToList()) {
            actor.Update();
        }
    }

    private readonly List<Actor> _actors = new();

    private readonly SquareArray<Item?> _items;

    private readonly SquareArray<Tile> _tiles;
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using uk.osric.dungeon.library;

public sealed record Point(int X, int Y) {
    internal Point(Point other) {
        X = other.X;
        Y = other.Y;
    }
};

internal static class PointExtensions {

    internal static double AngleTo(this Point from, Point to)
        => Math.Atan2(to.Y - from.Y, to.X - from.X);

    internal static Point Clamped(this Point p, int left, int top, int width, int height) {
        int x = p.X;
        int y = p.Y;
        if (p.X < left) {
            x = left;
        }

        if (p.X >= left + width) {
            x = left + width - 1;
        }

        if (p.Y < top) {
            y = top;
        }

        if (p.Y >= top + height) {
            y = top + height - 1;
        }

        return new(x, y);
    }

    internal static double Dot(this Point from, Point to)
        => (from.X * to.X) + (from.Y * to.Y);

    internal static Facing GetFacing(this Point p) {
        if (p.X >= p.Y && p.X >= -p.Y) {
            return Facing.Right;
        } else if (p.X >= p.Y && p.X < -p.Y) {
            return Facing.Up;
        } else if (p.X < p.Y && p.X >= -p.Y) {
            return Facing.Down;
        } else if (p.X < p.Y && p.X < -p.Y) {
            return Facing.Left;
        } else {
            throw new ArgumentOutOfRangeException(nameof(p));
        }
    }

    internal static bool IsInside(this Point p, Room r)
        => p.X >= r.Left && p.X < r.Left + r.Width && p.Y >= r.Top && p.Y < r.Top + r.Height;

    internal static double Magnitude(this Point point)
                    => Math.Sqrt((point.X * point.X) + (point.Y * point.Y));

    internal static IEnumerable<Point> Neighbors4(this Point p) {
        List<Point> points = new() {
            new Point(p.X - 1, p.Y),
            new Point(p.X + 1, p.Y),
            new Point(p.X, p.Y - 1),
            new Point(p.X, p.Y + 1)
        };

        points.Shuffle();
        return points;
    }

    internal static IEnumerable<Point> Neighbors8(this Point p) {
        List<Point> points = new();

        for (int dx = -1; dx < 2; dx += 1) {
            for (int dy = -1; dy < 2; dy += 1) {
                if (dx == 0 && dy == 0) {
                    continue;
                }

                points.Add(new Point(p.X + dx, p.Y + dy));
            }
        }

        points.Shuffle();
        return points;
    }
}

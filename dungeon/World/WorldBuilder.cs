﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using MIConvexHull;

internal class WorldBuilder {

    public WorldBuilder(int width, int height) {
        _width = width;
        _height = height;
        _tiles = new SquareArray<Tile>(width, height);
    }

    public World Build() => new(_tiles, _width, _height);

    public WorldBuilder MakeCaves() => RandomizeTiles().Smooth(17).FillHoles();

    public WorldBuilder MakeRooms(int depth, int factor) {
        static Tree SplitRoom(Room room, int depth) {
            if (depth > 0) {
                (Room l, Room r) = DoSplit(room, room.Width / room.Height > 1);
                Tree left = SplitRoom(l, depth - 1);
                Tree right = SplitRoom(r, depth - 1);
                return new(left, right, room);
            }

            return new(null, null, room);
        }

        static (Room, Room) DoSplit(Room parent, bool isVertical) {
            Room left, right;
            int factor = Random.Shared.Next(2, 5);
            if (isVertical) {
                left = new(parent.Left, parent.Top, parent.Width / factor, parent.Height);
                right = new(parent.Left + left.Width, parent.Top, parent.Width - left.Width, parent.Height);
            } else {
                left = new(parent.Left, parent.Top, parent.Width, parent.Height / factor);
                right = new(parent.Left, parent.Top + left.Height, parent.Width, parent.Height - left.Height);
            }

            return (left, right);
        }

        static void DrawRoom(SquareArray<Tile> tiles, Room room) {
            for (int col = 1; col < room.Width - 1; col += 1) {
                for (int row = 1; row < room.Height - 1; row += 1) {
                    tiles[room.Left + col, room.Top + row] = Tiles.Floor;
                }
            }
        }

        static Room RandomlySmaller(Room room, int factor) => new(
            room.Left + Random.Shared.Next(room.Width / factor),
            room.Top + Random.Shared.Next(room.Height / factor),
            room.Width - Random.Shared.Next(room.Width / factor),
            room.Height - Random.Shared.Next(room.Height / factor)
            );

        _tiles.ChangeEvery(t => Tiles.Unknown);

        Tree root = SplitRoom(new(0, 0, _width, _height), depth);

        IList<Room> rooms = root.Leaves
            .Where(r => r.AspectRatio > 0.6 && Random.Shared.Next(5) > 1)
            .Select(r => RandomlySmaller(r, factor).Fit(r.Left, r.Top, r.Width, r.Height))
            .ToList();

        foreach (Room r in rooms) {
            DrawRoom(_tiles, r);
        }

        ITriangulation<DefaultVertex, DefaultTriangulationCell<DefaultVertex>> triangles = Triangulation.CreateDelaunay(rooms.Select(r => new DefaultVertex() { Position = new double[] { r.Center.X, r.Center.Y } }).ToList());

        foreach (DefaultTriangulationCell<DefaultVertex> cell in triangles.Cells) {
            Point p1 = cell.Vertices[0].ToPoint();
            Point p2 = cell.Vertices[1].ToPoint();
            Point p3 = cell.Vertices[2].ToPoint();

            Room r1 = rooms.Single(r => p1.IsInside(r));
            Room r2 = rooms.Single(r => p2.IsInside(r));
            Room r3 = rooms.Single(r => p3.IsInside(r));

            Corridor c1 = new(r1, r2);
            Corridor c2 = new(r2, r3);
            Corridor c3 = new(r3, r1);

            foreach (Corridor c in new Corridor[] { c1, c2, c3 }) {
                c.Draw(_tiles);
            }
        }

        return this;
    }

    public WorldBuilder MakeSimple() {
        _tiles.ChangeEvery(t => Tiles.Floor);

        return this;
    }

    private record Tree(Tree? Left, Tree? Right, Room Room) {
        public IEnumerable<Room> Leaves {
            get {
                List<Room> rooms = new();
                WalkLeaves(r => rooms.Add(r));
                return rooms;
            }
        }

        public void WalkLeaves(Action<Room> action) {
            if (Left != null || Right != null) {
                Left?.WalkLeaves(action);
                Right?.WalkLeaves(action);
            } else {
                action(Room);
            }
        }
    }

    internal WorldBuilder DrawToConsole() {
        for (int row = 0; row < _height; row += 1) {
            for (int col = 0; col < _width; col += 1) {
                Console.Write(_tiles[col, row].Glyph);
            }

            Console.WriteLine();
        }

        return this;
    }

    private readonly int _height;

    private readonly int _width;

    private SquareArray<Tile> _tiles;

    private int CountWalls(SquareArray<Tile> tiles, int col, int row, int r) {
        int walls = 0;

        for (int dx = -r; dx < r + 1; dx += 1) {
            for (int dy = -r; dy < r + 1; dy += 1) {
                if (col + dx < 0 || col + dx >= _width || row + dy < 0 || row + dy >= _height) {
                    walls += 1;
                    continue;
                }

                if (tiles[col + dx, row + dy] == Tiles.Wall) {
                    walls += 1;
                }
            }
        }

        return walls;
    }

    private WorldBuilder FillHoles() {
        static int FillRegion<T>(SquareArray<T> a, T value, int startCol, int startRow) {
            int count = 0;

            Queue<Point> queue = new();
            queue.Enqueue(new(startCol, startRow));

            while (queue.Count > 0) {
                Point p = queue.Dequeue();

                if (!a.TryGet(p.X, p.Y, out T? current) || current == null) {
                    continue;
                }

                if (current.Equals(value)) {
                    continue;
                }

                a[p.X, p.Y] = value;

                foreach (Point n in p.Neighbors4()) {
                    queue.Enqueue(n);
                }
            }

            return count;
        }

        SquareArray<int> regions = new(_width, _width, -1);

        Dictionary<int, int> sizes = new();

        for (int col = 0; col < _width; col += 1) {
            for (int row = 0; row < _height; row += 1) {
                if (_tiles[col, row] == Tiles.Floor && regions[col, row] == -1) {
                    int index = sizes.Count;
                    int size = FillRegion(regions, index, col, row);
                    sizes.Add(index, size);
                }
            }
        }

        int largestRegionIndex = sizes.First(outer => outer.Value == sizes.Max(inner => inner.Value)).Key;

        for (int i = 0; i < regions._backingArray.Length; i += 1) {
            int r = regions._backingArray[i];
            if (r != -1 && r != largestRegionIndex) {
                _tiles._backingArray[i] = Tiles.Wall;
            }
        }

        return this;
    }

    private WorldBuilder RandomizeTiles() {
        _tiles.ChangeEvery(t => Random.Shared.Next(100) > 45 ? Tiles.Floor : Tiles.Wall);

        return this;
    }

    private WorldBuilder Smooth(int times) {
        SquareArray<Tile> working = new(_tiles.Width, _tiles.Height);

        while (times-- > 0) {
            for (int col = 0; col < _width; col += 1) {
                for (int row = 0; row < _height; row += 1) {
                    int walls = CountWalls(_tiles, col, row, 1);

                    // examine neighbours
                    if (walls >= 5) {
                        working[col, row] = Tiles.Wall;
                    } else {
                        working[col, row] = Tiles.Floor;
                    }
                }
            }

            _tiles = working;
        }

        return this;
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.library;

internal class Item : IDrawable {

    public Item(string name, char glyph, Style style, int attackValue = 0, int defenseValue = 0, int foodValue = 0, int thrownAttackValue = 1, int rangedAttackValue = 0) {
        Name = new Noun(name);
        Glyph = glyph;
        Style = style;
        MeleeAtackValue = attackValue;
        DefenseValue = defenseValue;
        FoodValue = foodValue;
        RangedAttackValue = rangedAttackValue;
        ThrownAttackValue = thrownAttackValue;
    }

    public char Glyph { get; private init; }

    public bool IsBreakable => QuaffEffect != null;

    public Noun Name { get; private init; }

    public Effect? QuaffEffect { get; set; }

    public Style Style { get; private init; }

    // Only potions break, for now

    internal int DefenseValue { get; set; }

    internal string Details => string.Join(", ", new string[] {
        MeleeAtackValue > 0 ? $"Attack (melee): {MeleeAtackValue}" : "",
        ThrownAttackValue > 0 ? $"Attack (thrown): {ThrownAttackValue}" :"",
        DefenseValue > 0 ? $"Defense: {DefenseValue}" : "",
        FoodValue > 0 ? $"Energy: {FoodValue}" : ""
        }.Where(s => !string.IsNullOrEmpty(s))

        );

    internal int FoodValue { get; private init; }

    internal int MeleeAtackValue { get; set; }

    internal int RangedAttackValue { get; set; }

    internal int ThrownAttackValue { get; set; }
}

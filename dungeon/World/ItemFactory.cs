﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using uk.osric.dungeon.display;
using uk.osric.dungeon.library;

internal static class ItemFactory {


    internal static Item Bow
        => new("bow", ')', new(AnsiColor.ForegroundYellow), attackValue: 6, defenseValue: 2, rangedAttackValue: 10);

    internal static Item Dagger
        => new("dagger", '\u2020', new(AnsiColor.ForegroundYellow), attackValue: 5, defenseValue: 1, thrownAttackValue: 4);

    internal static Item HeavyArmor
        => new("chainmail", '\u2624', new(AnsiColor.BrightForegroundGreen), defenseValue: 8);

    internal static Item LightArmor => new("tunic", '\u2624', new(AnsiColor.ForegroundGreen), defenseValue: 3);

    internal static Item PotionOfBattle
        => new("Potion of Battle", '!', new(AnsiColor.ForegroundCyan)) {
            QuaffEffect = new(20) {
                OnStart = a => {
                    a.AttackValue += 5;
                    a.DefenseValue += 5;
                    a.DoAction(Verb.Apply, "stronger");
                },
                OnEnd = a => {
                    a.AttackValue -= 5;
                    a.DefenseValue -= 5;
                    a.DoAction(Verb.Apply, "weaker");
                },
            }
        };

    internal static Item PotionOfHealth
        => new("Potion of Health", '!', new(AnsiColor.ForegroundCyan)) {
            QuaffEffect = new(1) {
                OnStart = a => {
                    if (a.Hp < a.MaxHp) {
                        a.Hp += 15;
                        a.DoAction(Verb.Apply, "healthier");
                    }
                }
            }
        };

    internal static Item PotionOfPoison
        => new("Potion of Poison", '!', new(AnsiColor.ForegroundCyan)) {
            QuaffEffect = new(20) {
                OnStart = a => a.DoAction(Verb.Apply, "sick"),
                OnUpdate = a => {
                    a.Hp -= 1;
                    a.DoAction(Verb.Apply, "sick");
                },
            }
        };

    internal static Item RandomArmor => Random.Shared.Next(2) switch {
        0 => LightArmor,
        1 => HeavyArmor,
        _ => LightArmor,
    };

    internal static Item RandomPotion => Random.Shared.Next(3) switch {
        0 => PotionOfHealth,
        1 => PotionOfPoison,
        _ => PotionOfBattle
    };

    internal static Item RandomWeapon => Random.Shared.Next(4) switch {
        0 => Dagger,
        1 => Sword,
        2 => Staff,
        3 => Bow,
        _ => Dagger,
    };

    internal static Item Staff => new("staff", '|', new(AnsiColor.ForegroundYellow), attackValue: 8, defenseValue: 4, thrownAttackValue: 4);

    internal static Item Sword => new("sword", '\u2020', new(AnsiColor.BrightForegroundYellow), attackValue: 10, defenseValue: 2, thrownAttackValue: 5);

    internal static Item TravelRation => new("travel ration", '=', new(AnsiColor.ForegroundMagenta), foodValue: 80);

    internal static Item Rock() => new("rock", ',', new(AnsiColor.ForegroundMagenta), attackValue: 1, thrownAttackValue: 3);
}

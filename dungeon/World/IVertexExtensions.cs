﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

using MIConvexHull;

internal static class IVertexExtensions {

    internal static Point ToPoint(this IVertex vertex, Func<double, double> round)
        => new((int)round(vertex.Position[0]), (int)round(vertex.Position[1]));

    internal static Point ToPoint(this IVertex vertex) => ToPoint(vertex, Math.Round);
}

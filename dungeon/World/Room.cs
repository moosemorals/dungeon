﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

internal record Room(int Left, int Top, int Width, int Height) {
    public Point Center => new(Left + (Width / 2), Top + (Height / 2));
    public double AspectRatio => Width > Height ? Height / (double)Width : Width / (double)Height;
}

internal static class RoomExtensions {

    public static Room Fit(this Room room, int left, int top, int width, int height) => room with {
        Left = Math.Max(left, room.Left),
        Top = Math.Max(top, room.Top),
        Width = room.Left + room.Width > left + width ? left + width - room.Left : room.Width,
        Height = room.Top + room.Height > top + height ? top + height - room.Top : room.Height
    };

    internal static Point Door(this Room Room, Facing Facing) => Facing switch {
        Facing.Up => new Point(Room.Center.X, Room.Top),
        Facing.Right => new Point(Room.Left + Room.Width, Room.Center.Y),
        Facing.Down => new Point(Room.Center.X, Room.Top + Room.Height),
        Facing.Left => new Point(Room.Left, Room.Center.Y),
        _ => throw new ArgumentOutOfRangeException(nameof(Facing))
    };
}

public enum Facing {

    Up, Right, Down, Left
}

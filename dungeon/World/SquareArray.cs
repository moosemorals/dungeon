﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

internal class SquareArray<T> {

    internal SquareArray(int width, int height, T? fill = default) {
        _backingArray = new T[width * height];
        Width = width;
        Height = height;

        if (fill is not null) {
            for (int i = 0; i < _backingArray.Length; i += 1) {
                _backingArray[i] = fill;
            }
        }
    }

    internal int Height { get; private init; }

    internal T this[int x, int y] {
        get {
            if (x < 0 || x >= Width) {
                throw new ArgumentOutOfRangeException(nameof(x));
            }

            if (y < 0 || y >= Height) {
                throw new ArgumentOutOfRangeException(nameof(y));
            }

            return _backingArray[(y * Width) + x];
        }

        set {
            if (x < 0 || x >= Width) {
                throw new ArgumentOutOfRangeException(nameof(x));
            }

            if (y < 0 || y >= Height) {
                throw new ArgumentOutOfRangeException(nameof(y));
            }

            _backingArray[(y * Width) + x] = value;
        }
    }

    internal int Width { get; private init; }

    internal readonly T[] _backingArray;

    internal void ChangeEvery(Func<T, T> func) {
        for (int i = 0; i < _backingArray.Length; i += 1) {
            _backingArray[i] = func(_backingArray[i]);
        }
    }

    internal Point? Find(T value) {
        for (int i = 0; i < _backingArray.Length; i += 1) {
            if (_backingArray[i]?.Equals(value) ?? false) {
                (int x, int y) = IndexToCords(i);
                return new(x, y);
            }
        }

        return null;
    }

    internal bool TryGet(int x, int y, out T? result) {
        if (x < 0 || x >= Width || y < 0 || y >= Height) {
            result = default;
            return false;
        }

        result = this[x, y];
        return true;
    }

    internal void VisitEvery(Action<T> action) {
        for (int i = 0; i < _backingArray.Length; i += 1) {
            action(_backingArray[i]);
        }
    }

    private (int x, int y) IndexToCords(int index)
                    => (index % Width, index / Width);
}

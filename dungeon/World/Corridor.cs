﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.world;

internal record Corridor(Room A, Room B);

internal static class CorridorExtensions {

    internal static void Draw(this Corridor c, SquareArray<Tile> tiles) {
        Point AB = new(c.A.Center.X - c.B.Center.X, c.A.Center.Y - c.B.Center.Y);
        Facing abf = AB.GetFacing();
        Point doorA = c.A.Door(abf);

        Point BA = new(c.B.Center.X - c.A.Center.X, c.B.Center.Y - c.A.Center.Y);
        Facing baf = BA.GetFacing();
        Point doorB = c.A.Door(baf);

        Point pivot = new(doorB.X, doorA.Y);

        foreach (Point p in new Line(doorA, pivot)) {
            tiles[p.X, p.Y] = Tiles.Floor;
        }

        foreach (Point p in new Line(pivot, doorB)) {
            tiles[p.X, p.Y] = Tiles.Floor;
        }
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.display;

internal interface IScreen {

    public void DisplayOutput(IDisplay display);

    public IScreen? OnUserInput(int key);
}

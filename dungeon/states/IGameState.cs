﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.states;

using uk.osric.dungeon.display;

internal interface IGameState {

    public void DrawOutput(IDisplay display);

    public IGameState? OnUserInput(int key);
}

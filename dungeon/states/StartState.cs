﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.states;

using uk.osric.dungeon.display;
using uk.osric.dungeon.library;

internal class StartState : IGameState {

    public void DrawOutput(IDisplay display) {
        display.Write(Ansi.ResetColor());

        int col = 5;
        int row = 4;

        foreach (PlayerCommand cmd in _playState.Commands) {
            IEnumerable<string> keys = cmd.Keys.Select(k => k >= 1000 ? "" : char.ConvertFromUtf32(k));
            display.Write(col, row++, $"{string.Join(' ', keys)}: {cmd.Name}");
        }

        display.Write(col, row + 2, "-- press [enter] to start --");
    }

    public IGameState OnUserInput(int key)
        => key switch {
            '\r' => _playState,
            _ => this,
        };

    internal StartState() {
        _playState = new();
    }

    private readonly PlayState _playState;
}

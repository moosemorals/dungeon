﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.states;

using System.Reflection;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.display;
using uk.osric.dungeon.library;
using uk.osric.dungeon.screens;
using uk.osric.dungeon.world;

internal class PlayState : IGameState {

    public void DrawOutput(IDisplay display) {
        // Draw play area
        DisplayTiles(display);

        // Draw a border round the play area
        display.Write(Ansi.ResetColor());
        if (_player.Hp < _player.MaxHp * 0.1 || _player.Energy < _player.MaxEnergy * 0.1) {
            display.SetColor(AnsiColor.BoldOn, AnsiColor.ForegroundRed);
        } else {
            display.SetColor(AnsiColor.ForegroundWhite);
        }

        display.DrawBox(0, 0, ScreenWidth + 2, ScreenHeight + 2);

        // Draw players stats
        display.Write(Ansi.ResetColor());
        string stats = string.Join(" | ",
            $"{_player.Hp}/{_player.MaxHp} hp (Level {_player.Level})",
            $"{Hunger(_player.Energy, _player.MaxEnergy)} ({(double)_player.Energy / _player.MaxEnergy:P0})",
            $"At ({_player.X},{_player.Y}) ");

        display.Write(2, ScreenHeight + 1, stats);

        // Check if the player is stood on something
        if (_world.GetItem(_player.X, _player.Y) is Item item) {
            _player.Notify($"There is a {item.Name} here");
        }

        // Show the player their messages
        for (int i = 0; i < _messages.Count; i += 1) {
            display.Write(1, ScreenHeight + i + 2, _messages[i]);
        }

        _messages.Clear();

        // Display subscreen
        _subScreen?.DisplayOutput(display);
    }

    public IGameState OnUserInput(int key) {
        int startingLevel = _player.Level;

        if (_subScreen != null) {
            _subScreen = _subScreen.OnUserInput(key);
        } else if (key == '\x1B') {
            // Player pressed 'esc'
            return new EndState("Quit - Thanks for playing");
        } else {
            // see if the player triggered a command

            foreach (PlayerCommand cmd in Commands) {
                if (cmd.Keys.Contains(key)) {
                    cmd.Action();
                    break;
                }
            }

            _world.Update();
        }

        if (_player.Level > startingLevel) {
            _subScreen = new LevelUpScreen(_player, _player.Level - startingLevel);
        }

        // After level up sceen as one of the levelling options
        // is restore health
        if (_player.Hp < 1) {
            return new EndState("You died");
        }

        return this;
    }

    internal PlayState() {
        ScreenWidth = 80;
        ScreenHeight = 21;
        _world = CreateWorld();

        _playerFov = new FieldOfView(_world);
        _player = _world.ActorFactory.Player(_playerFov, _messages);
        _world.AddAtEmptyLocation(_player);

        _world.Populate(_player);

        Commands = new() {
            new ("Move left", () => _player.Move(-1, 0), 'a', (int)KeyCodes.ArrowLeft),
            new ("Move right", () => _player.Move(1, 0), 'd', (int)KeyCodes.ArrowRight),
            new ("Move up", () => _player.Move(0, -1), 'w', (int)KeyCodes.ArrowUp),
            new ("Move down", () => _player.Move(0, 1), 's', (int)KeyCodes.ArrowDown),
            new ("Pickup item", () => _player.Pickup(), 'y', 'g'),
            new ("Rest (trade energy for health)", () => _player.Rest(), 'z'),
            };

        Commands.AddRange(GetScreenTypes().Select(t => MakeScreenCommand(t)));
    }

    internal List<PlayerCommand> Commands { get; private init; }

    internal int ScreenHeight { get; private set; }

    internal int ScreenWidth { get; private set; }

    internal int ScrollX => Math.Max(0, Math.Min(_player.X - ScreenWidth / 2, _world.Width - ScreenWidth));

    internal int ScrollY => Math.Max(0, Math.Min(_player.Y - ScreenHeight / 2, _world.Height - ScreenHeight));

    internal (int screenX, int screenY) ToScreenCords(int worldX, int worldY) => (worldX - ScrollX + _offsetX, worldY - ScrollY + _offsetY);

    internal (int worldX, int worldY) ToWorldCords(int screenX, int screenY) => (screenX + ScrollX - _offsetX, screenY + ScrollY - _offsetY);

    internal Point ToWorldCords(Point screen)
        => new(screen.X + ScrollX - _offsetX, screen.Y + ScrollY - _offsetY);

    private readonly List<string> _messages = new();

    private readonly int _offsetX = 1;

    private readonly int _offsetY = 1;

    private readonly Actor _player;

    private readonly FieldOfView _playerFov;

    private readonly World _world;

    private IScreen? _subScreen;

    private static IEnumerable<Type> GetScreenTypes()
        => Assembly.GetExecutingAssembly()
            .GetTypes()
            .Where(t => t.GetCustomAttribute<ScreenAttribute>() != null);

    private static string Hunger(int energy, int maxEnergy) => ((double)energy / maxEnergy) switch {
        < 0.1 => "Starving",
        < 0.3 => "Hungry",
        > 0.9 => "Stuffed",
        > 0.8 => "Full",
        _ => "Fine",
    };

    private World CreateWorld() {
        return new WorldBuilder(ScreenWidth * 2, ScreenHeight * 2)
            .MakeCaves()
            .Build();
    }

    private void DisplayTiles(IDisplay display) {
        _playerFov.Update(_player.X, _player.Y, _player.VisionRadius);

        int scrollX = ScrollX;
        int scrollY = ScrollY;

        // Draw the world
        Style? prevStyle = null;
        for (int row = 0; row < ScreenHeight; row += 1) {
            display.SetCursorPos(_offsetX, row + _offsetY);
            for (int col = 0; col < ScreenWidth; col += 1) {
                int worldX = col + scrollX;
                int worldY = row + scrollY;

                IDrawable d;
                Style drawStyle;
                if (_player.CanSee(worldX, worldY)) {
                    d = _world.GetDrawable(worldX, worldY);
                    drawStyle = d.Style.Bold();
                } else {
                    d = _playerFov.GetTile(worldX, worldY);
                    drawStyle = Style.OutOfRange;
                }

                if (drawStyle != prevStyle) {
                    display.SetColor(drawStyle.ToAnsi());
                    prevStyle = drawStyle;
                }

                display.Write(d.Glyph);
            }
        }

        // Draw actors in view
        foreach (Actor a in _world.Actors) {
            if (a.Inside(scrollX, scrollY, ScreenWidth, ScreenHeight)) {
                if (_player.CanSee(a.X, a.Y)) {
                    display.SetCursorPos(a.X - scrollX + _offsetX, a.Y - scrollY + _offsetY);
                    display.SetColor(a.Style.ToAnsi());
                    display.Write(a.Glyph);
                }
            }
        }
    }

    private IScreen MakeScreen(Type screenType) {
        ConstructorInfo ci = screenType.GetConstructors().Single();
        ParameterInfo[] pi = ci.GetParameters();
        object[] param = new object[pi.Length];

        for (int i = 0; i < pi.Length; i += 1) {
            ParameterInfo p = pi[i];
            if (p.ParameterType == typeof(Actor)) {
                param[i] = _player;
            } else if (p.ParameterType == typeof(PlayState)) {
                param[i] = this;
            }
        }

        return (IScreen)ci.Invoke(param);
    }

    private PlayerCommand MakeScreenCommand(Type screenType) {
        if (screenType.GetCustomAttribute<ScreenAttribute>() is not ScreenAttribute screenAttribute) {
            throw new ArgumentException("Missing ScreenAtribute", nameof(screenType));
        }

        return new PlayerCommand(screenAttribute.Description, () => _subScreen = MakeScreen(screenType), screenAttribute.KeyCodes);
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.states;

using uk.osric.dungeon.display;

internal class EndState : IGameState {

    public void DrawOutput(IDisplay display) {
        display.Write(1, 1, _message);
        display.WriteCenter("-- press [enter] to restart or [esc] to quit --");
    }

    public IGameState? OnUserInput(int key) {
        return key switch {
            '\r' => new PlayState(),
            '\x1B' => null,
            _ => this,
        };
    }

    internal EndState(string message) {
        _message = message;
    }

    private readonly string _message;
}

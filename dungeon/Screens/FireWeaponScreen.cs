﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.states;
using uk.osric.dungeon.world;

[Screen("Fire ranged weapon", 'F')]
internal class FireWeaponScreen : TargetBasedScreen {

    public FireWeaponScreen(Actor player, PlayState playState)
        : base(player, playState, $"Fire {player.Weapon?.Name} at") { }

    protected override bool IsAcceptable(Point offset) {
        Point target = new(Player.X + offset.X, Player.Y + offset.Y);

        if (!Player.CanSee(target.X, target.Y)) {
            return false;
        }

        foreach (Point p in new Line(Player.X, Player.Y, target.X, target.Y)) {
            if (!Player.GetTile(p.X, p.Y).IsGround) {
                return false;
            }
        }

        return true;
    }

    protected override IScreen? SelectWorldCoordinate(Point offset) {
        Point target = new(Player.X + offset.X, Player.Y + offset.Y);

        Actor? other = Player.GetActor(target.X, target.Y);
        if (other != null) {
            Player.RangedAttack(other);
        } else {
            Player.Notify("You fire at an empty space doing 0 damage");
        }

        return null;
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.library;
using uk.osric.dungeon.states;
using uk.osric.dungeon.world;

[Screen("Look far away", 'L')]
internal class LookScreen : TargetBasedScreen {

    public LookScreen(Actor player, PlayState playState, string caption) : base(player, playState, caption) { }

    protected override IScreen? EnterWorldCoordinate(Point target) {
        IDrawable? subject = Player.GetActor(target.X, target.Y);

        // If there's no creature, get an item
        subject ??= Player.GetItem(target.X, target.Y);

        // If there's no item, then fall back to tile
        subject ??= Player.GetVisibleTile(target.X, target.Y);

        Caption = $"{subject.Glyph}: {subject.Name}";

        return this;
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.world;

[Screen("Wield/Wear item", 'W')]
internal class EquipScreen : InventoryBaseScreen {

    public EquipScreen(Actor player) : base(player) { }

    protected override string Verb => "wear or weild";

    protected override bool IsAcceptable(Item item) => item.MeleeAtackValue > 0 || item.DefenseValue > 0;

    protected override IScreen? Use(Item item) {
        Player.Equip(item);
        return null;
    }
}

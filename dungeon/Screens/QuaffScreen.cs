﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.world;

[Screen("Quaff a potion", 'Q')]
internal class QuaffScreen : InventoryBaseScreen {

    public QuaffScreen(Actor player) : base(player) { }

    protected override string Verb => "quaff";

    protected override bool IsAcceptable(Item item) => item.QuaffEffect != null;

    protected override IScreen? Use(Item item) {
        Player.Quaff(item);
        return null;
    }
}

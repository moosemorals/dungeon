﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.display;

[Screen("Show help", 'H', 'h', '?')]
internal class HelpScreen : IScreen {

    public void DisplayOutput(IDisplay display) {
    }

    public IScreen? OnUserInput(int key) => null;
}

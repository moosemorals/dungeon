﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.world;

[Screen("Eat item", 'E')]
internal class EatScreen : InventoryBaseScreen {

    public EatScreen(Actor player) : base(player) { }

    protected override string Verb => "eat";

    protected override bool IsAcceptable(Item item) => item.FoodValue > 0;

    protected override IScreen? Use(Item item) {
        Player.Eat(item);
        return null;
    }
}

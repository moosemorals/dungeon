﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.states;
using uk.osric.dungeon.world;

internal class ThrowAtScreen : TargetBasedScreen {

    public ThrowAtScreen(Actor player, PlayState playState, Item item)
        : base(player, playState, $"Throw {item.Name} at") {
        _item = item;
    }

    protected override bool IsAcceptable(Point lookat) {
        Point target = new(Player.X + lookat.X, Player.Y + lookat.Y);
        if (!Player.CanSee(target.X, target.Y)) {
            return false;
        }

        foreach (Point p in new Line(Player.X, Player.Y, target.X, target.Y)) {
            if (!Player.GetTile(p.X, p.Y).IsGround) {
                return false;
            }
        }

        return true;
    }

    protected override IScreen? SelectWorldCoordinate(Point lookat) {
        Player.Throw(_item, Player.X + lookat.X, Player.Y + lookat.Y);
        return null;
    }

    private readonly Item _item;
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.display;
using uk.osric.dungeon.library;

internal class LevelUpScreen : IScreen {

    public void DisplayOutput(IDisplay display) {
        List<string> lines = GetLines();

        if (lines.Count == 0) {
            lines.Add("You do not have any valid leveling options");
        }

        string title = "Congratulations!";
        string[] message = new[] {
            $"Pick a reward for leveling up",
            $"You have {_picks} {Grammar.Plural("pick", _picks)} left",
        };

        int top = 5;
        int left = 5;
        int width = new int[] {
            lines.Max(l => l.Length),
            message.Max(l => l.Length),
            title.Length
        }.Max() + 2;

        int height = lines.Count + message.Length;

        display.DrawBox(left, top, width + 2, height + 3);

        display.SetCursorPos(left + 2, top);
        display.Write($" {title} ");

        int line = 1;
        foreach (string m in message) {
            display.SetCursorPos(left + 1, top + line++);
            display.Write(Ansi.OverwriteChars(width));
            display.Write($" {m}");
        }

        // Add a blank line
        display.SetCursorPos(left + 1, top + line++);
        display.Write(Ansi.OverwriteChars(width));

        foreach (string l in lines) {
            display.SetCursorPos(left + 1, top + line++);
            display.Write(Ansi.OverwriteChars(width));
            display.Write($" {l}");
        }
    }

    public IScreen? OnUserInput(int key) {
        if (key == 0x1B) { // Escape key
            return null;
        }

        int index = _letters.IndexOf((char)key);
        if (index == -1) {
            _player.Notify("That wasn't a valid choice");
            return this;
        }

        if (index > LevelUpper.Options.Length) {
            _player.Notify("That wasn't a valid choice");
            return this;
        }

        LevelUpper.Options[index].Apply(_player);

        if (--_picks < 1) {
            return null;
        }

        return this;
    }

    internal LevelUpScreen(Actor player, int picks) {
        _player = player;
        _picks = picks;
    }

    private const string _letters = "abcdefghijklmnopqrstuvwxyz";

    private readonly Actor _player;

    private int _picks;

    private static List<string> GetLines()
        => LevelUpper.Options.Select((o, i) => $"{_letters[i]}: {o.Name} ").ToList();
}

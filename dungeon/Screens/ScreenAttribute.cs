﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
internal class ScreenAttribute : Attribute {

    internal ScreenAttribute(string description, params int[] keyCodes) {
        Description = description;
        KeyCodes = keyCodes;
    }

    internal string Description { get; private init; }

    internal int[] KeyCodes { get; private init; }
}

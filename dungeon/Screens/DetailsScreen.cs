﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.world;

[Screen("Inspect carried item", 'I')]
internal class DetailsScreen : InventoryBaseScreen {

    public DetailsScreen(Actor player) : base(player) { }

    protected override string Verb => "examine";

    protected override bool IsAcceptable(Item item) => true;

    protected override IScreen? Use(Item item) {
        Player.Notify($"It's a {item.Name}: {item.Details}");

        return null;
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.world;

[Screen("Drop item", 'D')]
internal class DropScreen : InventoryBaseScreen {

    public DropScreen(Actor player) : base(player) { }

    protected override string Verb => "drop";

    protected override bool IsAcceptable(Item item) => true;

    protected override IScreen? Use(Item item) {
        Player.Drop(item);
        return null;
    }
}

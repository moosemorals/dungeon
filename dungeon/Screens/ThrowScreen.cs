﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.states;
using uk.osric.dungeon.world;

[Screen("Throw item", 'T')]
internal class ThrowScreen : InventoryBaseScreen {

    public ThrowScreen(Actor player, PlayState playState) : base(player) {
        _playState = playState;
    }

    protected override string Verb => "throw";

    protected override bool IsAcceptable(Item item) => true;

    protected override IScreen? Use(Item item)
        => new ThrowAtScreen(Player, _playState, item);

    private readonly PlayState _playState;
}

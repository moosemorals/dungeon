﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.display;
using uk.osric.dungeon.library;
using uk.osric.dungeon.states;
using uk.osric.dungeon.world;

internal abstract class TargetBasedScreen : IScreen {

    public void DisplayOutput(IDisplay display) {
        (int sx, int sy) = PlayState.ToScreenCords(Player.X, Player.Y);
        display.Write(Ansi.ResetColor());
        display.SetColor(AnsiColor.ForegroundMagenta);
        foreach (Point p in new Line(sx, sy, sx + _lookat.X, sy + _lookat.Y)) {
            if (p.X < 0 || p.X >= PlayState.ScreenWidth || p.Y < 0 || p.Y >= PlayState.ScreenHeight) {
                continue;
            }

            display.Write(p.X, p.Y, "*");
        }

        display.Write(Ansi.ResetColor());
        display.Write(2, 1, Caption);
    }

    public IScreen? OnUserInput(int key) {
        Point updated;

        switch (key) {
            case '\x1B':
                return null;

            case '\r':
                return SelectWorldCoordinate(_lookat);

            case 'a':
            case (int)KeyCodes.ArrowLeft:
                updated = _lookat with { X = _lookat.X - 1 };
                break;

            case 'd':
            case (int)KeyCodes.ArrowRight:
                updated = _lookat with { X = _lookat.X + 1 };
                break;

            case 'w':
            case (int)KeyCodes.ArrowUp:
                updated = _lookat with { Y = _lookat.Y - 1 };
                break;

            case 's':
            case (int)KeyCodes.ArrowDown:
                updated = _lookat with { Y = _lookat.Y + 1 };
                break;

            default:
                updated = _lookat;
                break;
        }

        if (IsAcceptable(updated)) {
            _lookat = updated;
            return EnterWorldCoordinate(new Point(Player.X + _lookat.X, Player.Y + _lookat.Y));
        }

        return this;
    }

    internal TargetBasedScreen(Actor player, PlayState playState, string caption) {
        Player = player;
        Caption = caption;
        PlayState = playState;
    }

    protected readonly Actor Player;

    protected readonly PlayState PlayState;

    protected string Caption;

    protected virtual IScreen? EnterWorldCoordinate(Point target) => this;

    protected virtual bool IsAcceptable(Point target) => true;

    protected virtual IScreen? SelectWorldCoordinate(Point target) => null;

    private Point _lookat = new(0, 0);
}

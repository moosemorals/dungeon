﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.screens;

using uk.osric.dungeon.actors;
using uk.osric.dungeon.display;
using uk.osric.dungeon.world;

internal abstract class InventoryBaseScreen : IScreen {

    public void DisplayOutput(IDisplay display) {
        List<string> lines = GetLines();

        if (lines.Count == 0) {
            lines.Add("Your inventory is empty");
        }

        string message = $"Enter the letter of the item to {Verb}";

        int top = 5;
        int left = 5;
        int width = Math.Max(lines.Max(l => l.Length),
            message.Length);
        int height = lines.Count;
        display.DrawBox(left, top, width + 2, height + 3);

        display.SetCursorPos(left + 2, top);
        display.Write(Verb);

        for (int i = 0; i < height; i += 1) {
            int row = top + 1 + i;
            display.SetCursorPos(left + 1, row);
            display.Write(Ansi.OverwriteChars(width));
            display.Write(lines[i]);
        }

        display.SetCursorPos(left + 1, top + height + 1);
        display.Write(Ansi.OverwriteChars(width));
        display.Write(message);
    }

    public IScreen? OnUserInput(int key) {
        if (key == 0x1B) { // Escape key
            return null;
        }

        int index = _letters.IndexOf((char)key);
        if (index == -1) {
            Player.Notify("That wasn't a valid choice");
            return this;
        }

        if (index > Player.Inventory.Count) {
            Player.Notify("That wasn't a valid choice");
            return this;
        }

        Item? item = Player.Inventory[index];
        if (item == null) {
            Player.Notify("That wasn't a valid choice");
            return this;
        }

        if (!IsAcceptable(item)) {
            Player.Notify($"You can't {Verb} {item.Name}");
            return this;
        }

        return Use(item);
    }

    internal InventoryBaseScreen(Actor player) => Player = player;

    protected Actor Player { get; init; }

    protected abstract string Verb { get; }

    protected abstract bool IsAcceptable(Item item);

    protected abstract IScreen? Use(Item item);

    private const string _letters = "abcdefghijklmnopqrstuvwxyz";

    private List<string> GetLines() {
        List<string> result = new();

        for (int i = 0; i < Player.Inventory.Count; i += 1) {
            Item? item = Player.Inventory[i];
            if (item is null || !IsAcceptable(item)) {
                continue;
            }

            string line = $"{_letters[i]}: {item.Name} ({item.Glyph})";
            if (Player.IsEquipped(item)) {
                line += " (equipped)";
            }

            result.Add(line);
        }

        return result;
    }
}

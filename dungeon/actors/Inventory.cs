﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using System.Collections;

using uk.osric.dungeon.world;

internal class Inventory : IEnumerable<Item?> {

    public IEnumerator<Item?> GetEnumerator() => ((IEnumerable<Item?>)_items).GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => _items.GetEnumerator();

    internal Inventory(int size) {
        _items = new Item?[size];
    }

    internal int Count => _items.Length;

    internal bool IsFull => _items.All(i => i != null);

    internal Item? this[int i] => _items[i];

    internal bool Add(Item item) {
        for (int i = 0; i < _items.Length; i += 1) {
            if (_items[i] == null) {
                _items[i] = item;
                return true;
            }
        }

        return false;
    }

    internal bool Contains(Item item) => _items.Contains(item);

    internal bool Remove(Item item) {
        for (int i = 0; i < _items.Length; i += 1) {
            if (_items[i] == item) {
                _items[i] = null;
                return true;
            }
        }

        return false;
    }

    private readonly Item?[] _items;
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

internal class Effect {

    public Effect(int duration) {
        _duration = duration;
    }

    internal bool IsDone => _duration < 1;

    internal Action<Actor>? OnEnd { get; init; }

    internal Action<Actor>? OnStart { get; init; }

    internal Action<Actor>? OnUpdate { get; init; }

    internal void End(Actor actor)
        => OnEnd?.Invoke(actor);

    internal void Start(Actor actor)
        => OnStart?.Invoke(actor);

    internal void Update(Actor actor) {
        _duration -= 1;
        OnUpdate?.Invoke(actor);
    }

    private int _duration;
}

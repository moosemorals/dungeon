﻿// SPDX-Licence-Idenitfier: ISC

using System.Runtime.CompilerServices;

using uk.osric.dungeon.display;
using uk.osric.dungeon.display.MSWindows;

[assembly: InternalsVisibleTo("Tests")]

namespace uk.osric.dungeon;

internal class Program {

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "Part of external API")]
    public static async Task Main(string[] args) {
        IDisplay display = new WindowsConsole();
        await new DungeonEngine(display).RunAsync();
    }
}

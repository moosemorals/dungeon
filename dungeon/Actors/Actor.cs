﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.library;
using uk.osric.dungeon.world;

internal class Actor : ILocatable, IDrawable {

    public char Glyph { get; private init; }

    public Noun Name { get; private set; }

    public Style Style { get; private init; }

    public int X { get; set; }

    public int Y { get; set; }

    internal Actor(World world, string name, char glyph, Style style, Func<Actor, ActorAi> makeAi, int maxHp, int attack, int defense) {
        Ai = makeAi(this);

        AttackValue = attack;
        DefenseValue = defense;
        Glyph = glyph;
        Inventory = new(20);
        MaxHp = maxHp;
        Hp = maxHp;
        MaxEnergy = 600;
        Energy = MaxEnergy / 3 * 2;
        Name = name == "you" ? Noun.You : new Noun(name, IsPlayer: IsPlayer);
        Style = style;
        VisionRadius = 7;
        Level = 1;
        World = world;
    }

    internal ActorAi Ai { get; private set; }

    internal Item? Armor { get; private set; }

    internal int AttackValue {
        get => _attackValue
            + (Weapon?.MeleeAtackValue ?? 0)
            + (Armor?.MeleeAtackValue ?? 0);
        set => _attackValue = value;
    }

    internal int DefenseValue {
        get => _defenseValue
            + (Weapon != null ? Weapon.DefenseValue : 0)
            + (Armor != null ? Armor.DefenseValue : 0);
        set => _defenseValue = value;
    }

    internal List<Effect> Effects { get; private init; } = new();

    #region Activities

    internal void ApplyEffect(Effect e) {
        e.Start(this);
        Effects.Add(e);
    }

    internal void Die() {
        DoAction(new Verb("die"));
        World.Remove(this);
        World.AddItem(MakeCorpse(), X, Y);
        foreach (Item? item in Inventory) {
            if (item is not null) {
                Drop(item);
            }
        }
    }

    internal void Dig(int worldX, int worldY) {
        Energy -= 10;
        World.Dig(worldX, worldY);
        DoAction(new("dig"), "into the wall");
    }

    internal void Drop(Item item) {
        if (World.AddItem(item, X, Y)) {
            Remove(item);
            DoAction(new("drop"), item.Name);
        } else {
            Notify($"There is no space to drop {item.Name}");
        }
    }

    internal void Eat(Item item) {
        Remove(item);
        Energy += item.FoodValue;

        if (item.FoodValue > 0) {
            DoAction(new("eat"), item.Name);
        } else {
            Notify($"eat {item.Name} dubiously");
        }
    }

    internal void Equip(Item item) {
        if (!Inventory.Contains(item)) {
            if (Inventory.IsFull) {
                Notify($"You can't equip {item.Name}, your inventory is full");
                return;
            }

            World.Remove(item);
            Inventory.Add(item);
        }

        if (item.MeleeAtackValue == 0
            && item.RangedAttackValue == 0
            && item.DefenseValue == 0) {
            Notify($"You can't equip {item.Name}");
            return;
        }

        if (item.MeleeAtackValue >= item.DefenseValue) {
            // Call it a weapon
            Unequip(Weapon);
            Weapon = item;
            DoAction(new ("wield"), item.Name);
        } else {
            // Must be armor
            Unequip(Armor);
            Armor = item;
            DoAction(new("put on"), item.Name);
        }
    }

    internal void MeleeAttack(Actor other) {
        Noun weaponName = Weapon?.Name ?? new Noun("bare hand");
        Attack(other, AttackValue, $"attack {other.Name} with {weaponName}");
    }

    internal void Move(int dx, int dy) {
        if (dx == 0 && dy == 0) {
            return;
        }

        Actor? other = GetActor(X + dx, Y + dy);

        if (other is null) {
            Energy -= 1;
            Ai.OnEnter(X + dx, Y + dy, World.GetTile(X + dx, Y + dy));
        } else {
            MeleeAttack(other);
        }
    }

    internal void Pickup() {
        Item? item = World.GetItem(X, Y);
        if (item == null) {
            DoAction(new ("grab"), "uslessly at the ground");
        } else if (Inventory.IsFull) {
            Notify("Your inventory is full");
        } else {
            DoAction(new("pickup"), item.Name);
            World.RemoveItemAt(X, Y);
            Inventory.Add(item);
        }
    }

    internal void Quaff(Item item) {
        Remove(item);

        if (item.QuaffEffect != null) {
            DoAction(new ("quaff"), item.Name);
            ApplyEffect(item.QuaffEffect);
        } else {
            DoAction(new ("attempt to quaff"), item.Name);
            DoAction($"It has no obvious effect");
        }
    }

    internal void RangedAttack(Actor target) {
        if (Weapon == null || Weapon.RangedAttackValue == 0) {
            Notify("You don't have a weapon equipped for that");
            return;
        }

        Attack(target,
            (AttackValue / 2) + Weapon.RangedAttackValue,
            $"fire {Weapon.Name} at {target.Name}");
    }

    internal void Rest() {
        if (Energy > 0 && Hp < MaxHp) {
            Energy -= 1;
            Hp += 1;
        }

        DoAction(new ("rest"),"for a moment");
    }

    internal void Throw(Item item, int worldX, int worldY) {
        Remove(item);
        Energy -= 1;

        Point target = new(worldX, worldY);

        foreach (Point p in new Line(new(X, Y), target)) {
            if (!GetTile(p.X, p.Y).IsGround) {
                break;
            }

            target = p;
        }

        if (GetActor(target.X, target.Y) is Actor actor) {
            ThrowAttack(item, actor);
            if (item.QuaffEffect != null) {
                actor.ApplyEffect(item.QuaffEffect);
            }
        } else {
            DoAction(new Verb("throw"), item.Name);
        }

        if (!item.IsBreakable) {
            World.AddItem(item, target.X, target.Y);
        }
    }

    internal void Unequip(Item? item) {
        if (item == null) {
            return;
        }

        if (item == Armor) {
            Armor = null;
            DoAction(new Verb("remove"), item.Name);
        } else if (item == Weapon) {
            Weapon = null;
            DoAction(new Verb("put away"), item.Name);
        }
    }

    #endregion Activities

    internal int Energy {
        get => _energy;
        set {
            _energy = value;
            if (_energy > MaxEnergy) {
                _energy = MaxEnergy;
            } else if (_energy < 1 && IsPlayer) {
                _energy = 0;
                Notify("You feel weak from hunger");
                Hp -= 1;
            }
        }
    }

    internal int Hp {
        get => _hp;
        set {
            _hp = value;
            if (_hp > MaxHp) {
                _hp = MaxHp;
            } else if (_hp < 0) {
                Die();
            }
        }
    }

    internal Inventory Inventory { get; private init; }

    internal bool IsPlayer => Ai.IsPlayer;

    internal int Level { get; private set; }

    internal int MaxEnergy { get; set; }

    internal int MaxHp { get; set; }

    internal int VisionRadius { get; set; }

    internal Item? Weapon { get; private set; }

    internal int Xp {
        get => _xp;
        set {
            _xp = value;

            Notify($"You {(value > 0 ? "gain" : "loose")} {value} xp");

            while (_xp > (int)Math.Pow(Level, 1.5) * 20) {
                Level += 1;
                Ai.OnGainLevel();
                Hp += Level * 2;
                DoAction(new ("gain"), "a level");
            }
        }
    }

    internal void AddToWorld(Actor actor) => World.AddAtEmptyLocation(actor);

    internal void AddToWorld(Item item) => World.AddAtEmptyLocation(item);

    internal bool CanEnter(int worldX, int worldY) => World.CanEnter(worldX, worldY);

    internal bool CanSee(int worldX, int worldY) => Ai.CanSee(worldX, worldY);

    internal void DoAction(Verb verb) => RealDoAction(verb.First,verb.Second);
    internal void DoAction(Verb verb, Noun noun) => RealDoAction($"{verb.First} {noun.AsPlayer}", $"{verb.Second} {noun}");
    internal void DoAction(Verb verb, string phrase) => RealDoAction($"{verb.First} {phrase}", $"{verb.Second} {phrase}");

    internal void DoAction(string message) => RealDoAction(message, Grammar.MakeSecondPerson(message));
    private void RealDoAction(string playerMessage, string otherMessage) {

        int radius = 9;

        // Find creatures within notification distance
        for (int dx = -radius; dx < radius; dx += 1) {
            for (int dy = -radius; dy < radius; dy += 1) {
                if ((dx * dx) + (dy * dy) > radius * radius) {
                    continue;
                }

                Actor? actor = World.GetActor(X + dx, Y + dy);
                if (actor == null) {
                    continue;
                }

                if (actor == this) {
                    actor.Notify($"You {playerMessage}.");
                } else if (actor.CanSee(X + dx, Y + dy)) {
                    actor.Notify($"The {actor.Name.Word} {otherMessage}");
                }
            }
        }
    }

    internal Actor? GetActor(int worldX, int worldY) {
        if (CanSee(worldX, worldY)) {
            return World.GetActor(worldX, worldY);
        }

        return null;
    }

    internal Item? GetItem(int worldX, int worldY) {
        if (CanSee(worldX, worldY)) {
            return World.GetItem(worldX, worldY);
        }

        return null;
    }

    internal Tile GetTile(int worldX, int worldY) => World.GetTile(worldX, worldY);

    internal Tile GetVisibleTile(int worldX, int worldY) {
        if (CanSee(worldX, worldY)) {
            return World.GetTile(worldX, worldY);
        }

        return Ai.RememberedTile(worldX, worldY);
    }

    internal bool IsEquipped(Item item) => Weapon == item || Armor == item;

    internal void Notify(string message) => Ai.OnNotify(message);

    internal void Update() {
        if (Effects.Any()) {
            ApplyEffects();
        }

        Ai.OnUpdate();
    }

    private World World { get; init; }

    private int _attackValue;

    private int _defenseValue;

    private int _energy;

    private int _hp;

    private int _xp;

    private void ApplyEffects() {
        List<Effect> finished = new();

        foreach (Effect e in Effects) {
            e.Update(this);
            if (e.IsDone) {
                e.End(this);
                finished.Add(e);
            }
        }

        if (finished.Any()) {
            Effects.RemoveAll(e => finished.Contains(e));
        }
    }

    private void Attack(Actor target, int attackValue, string message) {
        Energy -= attackValue / 4;

        int attackAmount = Math.Max(0, attackValue - target.DefenseValue);
        attackAmount = Random.Shared.Next(attackAmount) + 1;
        DoAction($"{message} for {attackAmount} damage");

        target.Hp -= attackAmount;

        if (target.Hp < 1) {
            GainXp(target);
        }
    }

    private void GainXp(Actor other) {
        int amount = other.MaxHp
            + other.AttackValue
            + other.DefenseValue
            - (Level * 2);

        if (amount > 0) {
            Xp += amount;
        }
    }

    private Item MakeCorpse()
        => new($"{Name} corpse", '%', Style, 0, 0, MaxHp * 3);

    private void Remove(Item item) {
        Unequip(item);
        Inventory.Remove(item);
    }

    private void ThrowAttack(Item item, Actor target)
            => Attack(target, (AttackValue / 2) + item.ThrownAttackValue, $"throw {item.Name} at {target.Name}");
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

internal class BatAi : ActorAi {

    public BatAi(Actor actor) : base(actor) { }

    internal override void OnUpdate() {
        Wander();
        Wander();
    }
}

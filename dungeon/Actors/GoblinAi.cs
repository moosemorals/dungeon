﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.world;

internal class GoblinAi : ActorAi {

    internal GoblinAi(Actor actor, Actor player) : base(actor) {
        _player = player;
    }

    internal override void OnUpdate() {
        if (CanAttackWithRangedWeapon(_player)) {
            Actor.RangedAttack(_player);
        } else if (CanAttckWithThrownWeapon(_player)
            && GetThrowableWeapon() is Item item) {
            Actor.Throw(item, _player.X, _player.Y);
        } else if (CanPickupItem()) {
            Actor.Pickup();
            if (CheckInventoryForBetterArmor() is Item armor) {
                Actor.Equip(armor);
            }

            if (CheckInventoryForBetterWeapon() is Item weapon) {
                Actor.Equip(weapon);
            }
        } else if (Actor.CanSee(_player.X, _player.Y)) {
            Hunt(_player);
        } else {
            Wander();
        }
    }

    private readonly Actor _player;
}

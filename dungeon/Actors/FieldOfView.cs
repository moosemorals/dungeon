﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.world;

internal class FieldOfView {

    internal FieldOfView(World world) {
        _world = world;
        _visible = new(_world.Width, _world.Height);
        _memory = new(_world.Width, _world.Height, Tiles.Unknown);
    }

    internal Tile GetTile(int worldX, int worldY) => _memory[worldX, worldY];

    internal bool IsVisible(int worldX, int worldY) {
        if (_visible.TryGet(worldX, worldY, out bool v)) {
            return v;
        }

        return false;
    }

    internal void Update(int worldX, int worldY, int r) {
        _visible = new(_world.Width, _world.Height);

        for (int dx = -r; dx < r; dx += 1) {
            for (int dy = -r; dy < r; dy += 1) {
                if ((dx * dx) + (dy * dy) > r * r) {
                    continue;
                }

                if (worldX + dx < 0 || worldX + dx >= _world.Width || worldY + dy < 0 || worldY + dy >= _world.Height) {
                    continue;
                }

                foreach (Point p in new Line(worldX, worldY, worldX + dx, worldY + dy)) {
                    Tile t = _world.GetTile(p.X, p.Y);

                    _visible[p.X, p.Y] = true;
                    _memory[p.X, p.Y] = t;

                    if (!t.IsGround) {
                        break;
                    }
                }
            }
        }
    }

    private readonly SquareArray<Tile> _memory;

    private readonly World _world;

    private SquareArray<bool> _visible;
}

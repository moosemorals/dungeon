﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.library;

internal record LevelUpOption(string Name, Action<Actor> Apply);

internal static class LevelUpper {

    internal static LevelUpOption[] Options { get; } = new LevelUpOption[] {
        new ("Increase maximum health", p => {
            p.MaxHp += 10;
            p.Hp += 10;
            p.DoAction(Verb.Apply, "more resilient");
        }),
        new ("Increase attack strength", p => {
            p.AttackValue += 1;
            p.DoAction(Verb.Apply, "stronger");
        }),
        new ("Increase defense strength", p => {
            p.DefenseValue += 2;
            p.DoAction(Verb.Apply, "tougher");
        }),
        new ("Increase vision", p => {
            p.VisionRadius += 1;
            p.DoAction(Verb.Apply, "more aware");
        }),
        new ("Restore health", p=> {
            p.Hp = p.MaxHp;
            p.DoAction(Verb.Apply, "in the best of health");
        }),
        new ("Restore energy", p => {
            p.Energy = p.MaxEnergy;
            p.DoAction(Verb.Apply, "full of energy");
        }),
    };

    internal static void AutoLevelUp(Actor actor)
            => Options[Random.Shared.Next(Options.Length)].Apply(actor);
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.world;

internal class DeveloperAi : ActorAi {

    public DeveloperAi(Actor actor, FieldOfView fov, List<string> messages) : base(actor) {
        _fov = fov;
        _messages = messages;
    }

    internal override bool IsPlayer => true;

    internal override bool CanSee(int x, int y) => true;

    internal override void OnEnter(int x, int y, Tile tile) {
        if (tile.IsGround) {
            Actor.X = x;
            Actor.Y = y;
        } else if (tile.IsDiggable) {
            Actor.Dig(x, y);
        }
    }

    internal override void OnGainLevel() { }

    internal override void OnNotify(string message) => _messages.Add(message);

    internal override Tile RememberedTile(int worldX, int worldY) => _fov.GetTile(worldX, worldY);

    private readonly FieldOfView _fov;

    private readonly List<string> _messages;
}

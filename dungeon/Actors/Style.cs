// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.display;

internal record Style(
    AnsiColor Foreground = AnsiColor.ForegroundWhite,
    AnsiColor Background = AnsiColor.BackgroundBlack,
    bool IsBold = false,
    bool IsUnderline = false) {
    public static readonly Style Default = new();
    public static readonly Style OutOfRange = new(AnsiColor.ForegroundWhite);
}

internal static class StyleExtensions {

    internal static Style Bold(this Style style) => style with { IsBold = true };

    internal static Style Plain(this Style style) => style with { IsBold = false, IsUnderline = false };

    internal static AnsiColor[] ToAnsi(this Style style)
        => new AnsiColor[] {
            style.IsBold ? AnsiColor.BoldOn : AnsiColor.BoldOff,
            style.IsUnderline ? AnsiColor.UnderlineOn : AnsiColor.UnderlineOff,
            style.Foreground,
            style.Background
        }
        ;

    internal static Style UnderLine(this Style style) => style with { IsUnderline = true };
}

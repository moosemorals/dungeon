﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.display;
using uk.osric.dungeon.world;

internal class ActorFactory {

    public Actor Bat() => new(_world,
        "Bat", 'b',
        new(AnsiColor.ForegroundCyan, IsUnderline: true),
        b => new BatAi(b),
        15, 5, 0);

    public Actor Developer(FieldOfView fov, List<string> messages) => new(_world,
        "you",
        '!',
        new(AnsiColor.BrightForegroundGreen),
        a => new DeveloperAi(a, fov, messages),
        1000, 2000, 500);

    public Actor Fungus() => new(_world,
        "Fungus",
        'f',
        new(AnsiColor.ForegroundCyan),
        f => new FungusAi(f, this),
        10, 0, 0);

    public Actor Goblin(Actor player) {
        Actor goblin = new(_world,
            "goblin", 'g',
            new(AnsiColor.ForegroundGreen),
            g => new GoblinAi(g, player),
            66, 15, 5);

        goblin.Equip(ItemFactory.RandomWeapon);
        goblin.Equip(ItemFactory.RandomArmor);

        return goblin;
    }

    public Actor Player(FieldOfView fov, List<string> messages) => new(_world,
        "you",
        '@',
        new(AnsiColor.BrightForegroundRed),
        a => new PlayerAi(a, fov, messages),
        100, 20, 5);

    public Actor Zombie(Actor player) => new(_world,
        "zombie",
        'z',
        new(AnsiColor.ForegroundCyan, AnsiColor.BackgroundWhite),
        a => new ZombieAi(a, player),
        50, 10, 0);

    internal ActorFactory(World world) => _world = world;

    private readonly World _world;
}

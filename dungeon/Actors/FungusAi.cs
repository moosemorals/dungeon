﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

internal class FungusAi : ActorAi {

    public FungusAi(Actor actor, ActorFactory actorFactory) : base(actor) {
        _actorFactory = actorFactory;
    }

    internal override void OnUpdate() {
        if (_childCount < _maxChildren && Rand(20) == 0) {
            Spread();
        }
    }

    private readonly ActorFactory _actorFactory;

    private int _childCount;

    private int _maxChildren = 5;

    private void Spread() {
        int x = Actor.X + Random.Shared.Next(11) - 5;
        int y = Actor.Y + Random.Shared.Next(11) - 5;

        if (!Actor.CanEnter(x, y)) {
            return;
        }

        Actor child = _actorFactory.Fungus();

        ((FungusAi)child.Ai)._maxChildren = _maxChildren - 1;

        Actor.AddToWorld(child);
        Actor.DoAction(new ("spawn"),"a child");

        child.X = x;
        child.Y = y;
        _childCount += 1;
    }
}

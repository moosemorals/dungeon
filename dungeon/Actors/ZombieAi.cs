﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

internal class ZombieAi : ActorAi {

    public ZombieAi(Actor actor, Actor player) : base(actor) {
        _player = player;
    }

    internal override void OnUpdate() {
        if (Random.Shared.Next(5) == 0) {
            return;
        }

        if (Actor.CanSee(_player.X, _player.Y)) {
            Hunt(_player);
        } else {
            Wander();
        }
    }

    private readonly Actor _player;
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.world;

internal class Path {

    internal Path(Point start, Point goal) {
        Points = PathFinder.GetPath(start, goal,
            p => Math.Max(Math.Abs(p.X - goal.X), Math.Abs(p.Y - goal.Y)),
            (p1, p2) => 1
            );
    }

    internal Path(Actor preditor, Actor prey)
        : this(new Point(preditor.X, preditor.Y), new Point(prey.X, prey.Y)) { }

    internal IEnumerable<Point> Points { get; }
}

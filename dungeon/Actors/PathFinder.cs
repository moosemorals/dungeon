﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.world;

internal static class PathFinder {

    internal static IEnumerable<Point> GetPath(Point start, Point goal, Func<Point, double> heuristic, Func<Point, Point, double> distance) {
        // The set of points to try next
        PriorityQueue<Point, double> openSet = new();

        // For Point p, comeFrom[p] is the step towards the start on the cheapest path
        Dictionary<Point, Point> comeFrom = new();

        // For Point p, gScore[p] is the cost of the cheapest path from start to P
        Dictionary<Point, double> gScore = new();

        // For Point p, fScore[p] is our current best guess of the cost of a path through p
        Dictionary<Point, double> fScore = new();

        double GetGScore(Point p) => gScore.ContainsKey(p) ? gScore[p] : double.MaxValue;

        IEnumerable<Point> ReconstructPath(Point current) {
            List<Point> path = new() { current };
            while (comeFrom.ContainsKey(current)) {
                current = comeFrom[current];
                path.Add(current);
            }

            path.Reverse();

            return path.Skip(1);
        }

        gScore[start] = 0;
        fScore[start] = heuristic(start);
        openSet.Enqueue(start, fScore[start]);

        while (openSet.Count > 0) {
            Point current = openSet.Dequeue();
            if (current == goal) {
                return ReconstructPath(current);
            }

            foreach (Point neighbour in current.Neighbors4()) {
                double g = GetGScore(current) + distance(current, neighbour);
                if (g < GetGScore(neighbour)) {
                    // Path to this neighbour is better than any we've found so far
                    comeFrom[neighbour] = current;
                    gScore[neighbour] = g;
                    fScore[neighbour] = g + heuristic(neighbour);
                    if (!openSet.UnorderedItems.Any(i => i.Element == neighbour)) {
                        openSet.Enqueue(neighbour, g);
                    }
                }
            }
        }

        return new List<Point>();
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.actors;

using uk.osric.dungeon.world;

internal class ActorAi {

    public ActorAi(Actor actor) {
        Actor = actor;
    }

    internal virtual bool IsPlayer => false;

    internal virtual bool CanSee(int worldX, int worldY) {
        if (((Actor.X - worldX) * (Actor.X - worldX)) + ((Actor.Y - worldY) * (Actor.Y - worldY))
            > (Actor.VisionRadius * Actor.VisionRadius)) {
            return false;
        }

        foreach (Point p in new Line(Actor.X, Actor.Y, worldX, worldY)) {
            if (!Actor.GetTile(p.X, p.Y).IsGround && (p.X != worldX || p.Y != worldY)) {
                return false;
            }
        }

        return true;
    }

    internal virtual Tile RememberedTile(int worldX, int worldY) => Tiles.Unknown;

    protected readonly Actor Actor;

    protected static int Rand(int max) => Random.Shared.Next(max);

    protected Item? GetThrowableWeapon() {
        Item? candidate = null;

        foreach (Item item in Actor.Inventory.OfType<Item>()) {
            if (item == Actor.Weapon || item == Actor.Armor) {
                // Don't throw items that don't exist, or that we're
                // weilding
                continue;
            }

            if (candidate == null || item.ThrownAttackValue > candidate.ThrownAttackValue) {
                candidate = item;
            }
        }

        return candidate;
    }

    #region Events

    internal virtual void OnEnter(int worldX, int worldY, Tile tile) {
        if (tile.IsGround) {
            Actor.X = worldX;
            Actor.Y = worldY;
        } else {
            Actor.DoAction(new("bump"), "into a wall");
        }
    }

    internal virtual void OnGainLevel() => LevelUpper.AutoLevelUp(Actor);

    internal virtual void OnNotify(string message) { }

    internal virtual void OnUpdate() { }

    #endregion Events

    #region Tests

    protected bool CanAttackWithRangedWeapon(Actor target) {
        return Actor.Weapon != null
            && Actor.Weapon.RangedAttackValue > 0
            && Actor.CanSee(target.X, target.Y);
    }

    protected bool CanAttckWithThrownWeapon(Actor target) {
        return Actor.CanSee(target.X, target.Y)
            && GetThrowableWeapon() != null;
    }

    protected bool CanPickupItem() {
        return Actor.GetItem(Actor.X, Actor.Y) != null
            && !Actor.Inventory.IsFull;
    }

    protected Item? CheckInventory(Func<Item, bool> test) {
        foreach (Item item in Actor.Inventory.OfType<Item>()) {
            if (test(item)) {
                return item;
            }
        }

        return null;
    }

    protected Item? CheckInventoryForBetterArmor() {
        int currentArmorValue = Actor.Armor?.DefenseValue ?? 0;
        if (CheckInventory(i => i.DefenseValue > currentArmorValue) is Item item) {
            return item;
        }

        return null;
    }

    protected Item? CheckInventoryForBetterWeapon() {
        int currentWeaponValue = Actor.Weapon?.MeleeAtackValue ?? 0
            + Actor.Weapon?.RangedAttackValue ?? 0;
        if (CheckInventory(i => i.MeleeAtackValue + i.RangedAttackValue > currentWeaponValue) is Item item) {
            return item;
        }

        return null;
    }

    #endregion Tests

    #region Behaviours

    internal void Hunt(Actor target) {
        Point? step = new Path(Actor, target).Points.FirstOrDefault();

        if (step is not null) {
            int mx = step.X - Actor.X;
            int my = step.Y - Actor.Y;

            Actor.Move(mx, my);
        }
    }

    internal void Wander() {
        int dx = Random.Shared.Next(3) - 1;
        int dy = Random.Shared.Next(3) - 1;

        Tile tile = Actor.GetTile(Actor.X + dx, Actor.Y + dy);
        if (!tile.IsWalkable) {
            // Don't walk into walls
            return;
        }

        Actor? other = Actor.GetActor(Actor.X + dx, Actor.Y + dy);
        if (other != null && Actor.Glyph == other.Glyph) {
            // don't attack things that look like me
            return;
        }

        Actor.Move(dx, dy);
    }

    #endregion Behaviours
}

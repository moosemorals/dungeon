﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display;

public interface IDisplay : IDisposable {

    Task<uint> FlushAsync();

    (int row, int col) GetCursorPos();

    (int rows, int cols) GetSize();

    Task<int> ReadKeyAsync(CancellationToken token = default);

    void Reset();

    void Write(byte[] bytes, int offset, int length);
}

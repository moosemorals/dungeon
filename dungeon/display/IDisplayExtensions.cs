﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display;

using System.Text;

internal static class IDisplayExtensions {

    public static void Clear(this IDisplay display) {
        display.Write(Ansi.SetCursorPos(0, 0));
        display.Write(Ansi.ClearToEndOfDisplay());
    }

    public static IDisplay DrawBox(this IDisplay display, int left, int top, int width, int height) {
        // Top row
        display.SetCursorPos(left, top);
        display.Write(BoxDrawings.DoubleDownAndRight);
        for (int i = 1; i < width - 1; i += 1) {
            display.Write(BoxDrawings.DoubleHorizontal);
        }

        display.Write(BoxDrawings.DoubleDownAndLeft);

        // Sides
        for (int i = 1; i < height - 1; i += 1) {
            display.SetCursorPos(left, top + i);
            display.Write(BoxDrawings.DoubleVertical);
            display.SetCursorPos(left + width - 1, top + i);
            display.Write(BoxDrawings.DoubleVertical);
        }

        // Bottom
        display.SetCursorPos(left, top + height - 1);
        display.Write(BoxDrawings.DoubleUpAndRight);
        for (int i = 1; i < width - 1; i += 1) {
            display.Write(BoxDrawings.DoubleHorizontal);
        }

        display.Write(BoxDrawings.DoubleUpAndLeft);

        return display;
    }

    public static void SetColor(this IDisplay display, params AnsiColor[] colors) => display.Write(Ansi.SetColor(colors));

    public static void SetCursorPos(this IDisplay display, int col, int row) => display.Write(Ansi.SetCursorPos((short)col, (short)row));

    public static void Write(this IDisplay display, char c) => display.Write(_utf8.GetBytes(new char[] { c }));

    public static void Write(this IDisplay display, string text) => display.Write(_utf8.GetBytes(text));

    public static void Write(this IDisplay display, byte[] bytes) => display.Write(bytes, 0, bytes.Length);

    public static void Write(this IDisplay display, int col, int row, string text) {
        display.SetCursorPos(col, row);
        display.Write(text);
    }

    public static void WriteCenter(this IDisplay display, string text) {
        (int rows, int cols) = display.GetSize();

        display.SetCursorPos((short)((cols - text.Length) / 2), (short)(rows / 2));
        display.Write(text);
    }

    public static class BoxDrawings {

        internal const char DoubleDownAndLeft = '\u2557';

        internal const char DoubleDownAndRight = '\u2554';

        internal const char DoubleHorizontal = '\u2550';

        internal const char DoubleUpAndLeft = '\u255D';

        internal const char DoubleUpAndRight = '\u255A';

        internal const char DoubleVertical = '\u2551';
    }

    private static readonly Encoding _utf8 = new UTF8Encoding(false);
}

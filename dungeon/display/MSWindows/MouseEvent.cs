﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct MouseEvent {

    public Coord MousePosition;

    public MouseButtonState ButtonState;

    public ControlKeyState ControlKeyState;

    public MouseEventFlags EventFlags;
}

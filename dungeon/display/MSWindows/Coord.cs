﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct Coord {

    public short X;

    public short Y;

    public Coord(short x, short y) {
        X = x;
        Y = y;
    }
}

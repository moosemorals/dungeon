﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

public struct WindoBufferEvent {

    public WindoBufferEvent(short x, short y) {
        Size = new Coord(x, y);
    }

    public Coord Size;
}

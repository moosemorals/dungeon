﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct SmallRect {

    public short Left;

    public short Top;

    public short Right;

    public short Bottom;
}

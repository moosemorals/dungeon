﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;
using System.Text;

[Flags]
internal enum FormatMessageFlags : int {

    AllocateBuffer = 0x100,

    ArgumentArray = 0x2000,

    FromHModule = 0x800,

    FromString = 0x400,

    FromSystem = 0x1000,

    IgnoreInserts = 0x200,
}

internal class Kernel32 {

    public const uint CP_UTF8 = 65001;

    public const nint InvalidStdHandle = -1;

    public const nint StdErrHandle = -12;

    public const nint StdInHandle = -10;

    public const nint StdOutHandle = -11;

    [DllImport("coredll", CharSet = CharSet.Unicode, EntryPoint = "FormatMessageW", SetLastError = true)]
    internal static extern int FormatMessage(FormatMessageFlags flags, IntPtr source, int messageId, int languageId, StringBuilder buffer, int size, IntPtr[] Arguments);

    [DllImport(Kernel32Dll, SetLastError = true)]
    internal static extern bool GetConsoleMode(IntPtr consoleHandle, out uint mode);

    [DllImport(Kernel32Dll, SetLastError = true)]
    internal static extern bool GetConsoleScreenBufferInfo(IntPtr consoleHandle, out ConsoleScreenBufferInfo screenBufferInfo);

    [DllImport(Kernel32Dll, SetLastError = true)]
    internal static extern IntPtr GetStdHandle(nint StdHandle);

    [DllImport(Kernel32Dll, CharSet = CharSet.Unicode, SetLastError = true, EntryPoint = "ReadConsoleInputW")]
    internal static extern bool ReadConsoleInput(IntPtr consoleHandle, out InputEvent inputEvent, uint length, out uint count);

    [DllImport(Kernel32Dll, SetLastError = true)]
    internal static extern bool SetConsoleCP(uint codePageId);

    [DllImport(Kernel32Dll, SetLastError = true)]
    internal static extern bool SetConsoleMode(IntPtr consoleHandle, uint mode);

    [DllImport(Kernel32Dll, SetLastError = true)]
    internal static extern bool SetConsoleOutputCP(uint codePageId);

    [DllImport(Kernel32Dll, SetLastError = true)]
    internal static extern WaitResult WaitForSingleObject(IntPtr consoleHandle, short milliseconds);

    [DllImport(Kernel32Dll, CharSet = CharSet.Unicode, SetLastError = true, EntryPoint = "WriteConsoleA")]
    internal static extern bool WriteConsole(IntPtr ConsoleHandle, byte[] bytes, uint Length, out uint NumberOfCharsWritten);

    private const string Kernel32Dll = "kernel32.dll";
}

internal enum WaitResult : short {

    Abandoned = 0x80,

    Signaled = 0x00,

    Timeout = 0x102,

    Failed = -1,
}

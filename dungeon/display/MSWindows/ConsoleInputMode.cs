﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

[Flags]
public enum ConsoleInputMode : uint {

    /// <summary>
    /// Ctrl-C is handled by the system and not passed on
    /// </summary>
    EnableProcessedInput = 0x01,

    /// <summary>
    /// ReadFile and ReadConsole only return after newline
    /// </summary>
    EnableLineInput = 0x02,

    /// <summary>
    /// Characters read by ReadFile or ReadConsole are echoed to the screen
    /// </summary>
    EnableEcho = 0x04,

    /// <summary>
    /// ReadConsoleInput will report window events
    /// </summary>
    EnableWindowEvents = 0x08,

    /// <summary>
    /// ReadConsoleInput will report mouse events within the borders of the console
    /// </summary>
    EnableMouseEvents = 0x10,

    /// <summary>
    /// Text entered in console window will be inserted and not overwrite
    /// existing text
    /// </summary>
    EnableInsertMode = 0x20,

    /// <summary>
    /// Allow the user to use the mouse to select text
    /// </summary>
    EnableQuickEditMode = 0x40,

    EnableExtendedFlags = 0x80,

    EnableAutoPosition = 0x100,

    /// <summary>
    /// Convert user input to VT100 sequences
    /// </summary>
    EnableVirtualTemrinalInput = 0x200,
}

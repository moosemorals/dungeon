﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct FocusEvent {

    public uint SetFocus;
}

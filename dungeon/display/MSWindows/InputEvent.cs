﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Explicit)]
public struct InputEvent {

    [FieldOffset(0)]
    public InputEventType EventType;

    [FieldOffset(4)]
    public KeyEvent KeyEvent;

    [FieldOffset(4)]
    public MouseEvent MouseEvent;

    [FieldOffset(4)]
    public WindoBufferEvent WindowBufferSizeEvent;

    [FieldOffset(4)]
    public MenuEvent MenuEvent;

    [FieldOffset(4)]
    public FocusEvent FocusEvent;
};

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
public struct KeyEvent {

    [FieldOffset(0), MarshalAs(UnmanagedType.Bool)]
    public bool KeyDown;

    [FieldOffset(4), MarshalAs(UnmanagedType.U2)]
    public ushort RepeatCount;

    [FieldOffset(6), MarshalAs(UnmanagedType.U2)]
    public VirtualKey VirtualKeyCode;

    [FieldOffset(8), MarshalAs(UnmanagedType.U2)]
    public ushort VirtualScanCode;

    [FieldOffset(10)]
    public char Char;

    [FieldOffset(12), MarshalAs(UnmanagedType.U4)]
    public ControlKeyState ControlKeyState;
}

public static class KeyEventExtensions {

    public static bool IsCtrlPressed(this KeyEvent keyEvent) {
        return keyEvent.ControlKeyState.HasFlag(ControlKeyState.LeftCtl)
            || keyEvent.ControlKeyState.HasFlag(ControlKeyState.RightCtl);
    }
}

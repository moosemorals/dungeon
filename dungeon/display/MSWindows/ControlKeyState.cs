﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

[Flags]
public enum ControlKeyState : uint {

    RightAlt = 0x1,

    LeftAlt = 0x2,

    RightCtl = 0x4,

    LeftCtl = 0x8,

    Shift = 0x10,

    Numlock = 0x20,

    ScrollLock = 0x40,

    Capslock = 0x80,

    Enhanced = 0x100
}

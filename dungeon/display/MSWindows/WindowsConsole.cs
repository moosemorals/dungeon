﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;
using System.Text;

using uk.osric.dungeon.display;
using uk.osric.dungeon.library;

internal class WindowsConsole : IDisposable, IDisplay {

    public WindowsConsole() {
        _stdIn = Kernel32.GetStdHandle(Kernel32.StdInHandle);
        if (_stdIn == Kernel32.InvalidStdHandle) {
            throw Win32Error("Can't open stdin");
        }

        _stdOut = Kernel32.GetStdHandle(Kernel32.StdOutHandle);
        if (_stdOut == Kernel32.InvalidStdHandle) {
            throw Win32Error("Can't open stdout");
        }

        if (!Kernel32.GetConsoleMode(_stdIn, out _oldStdInMode)) {
            throw Win32Error("Can't get current std in mode");
        }

        if (!Kernel32.SetConsoleMode(_stdIn, (uint)StdInMode)) {
            throw Win32Error("Can't set std in mode");
        }

        if (!Kernel32.SetConsoleCP(Kernel32.CP_UTF8)) {
            throw Win32Error("Can't set input code page to UTF8");
        }

        if (!Kernel32.GetConsoleMode(_stdOut, out _oldStdOutMode)) {
            throw Win32Error("Can't get current std out mode");
        }

        if (!Kernel32.SetConsoleMode(_stdOut, (uint)StdOutMode)) {
            throw Win32Error("Can't set std out mode");
        }

        if (!Kernel32.SetConsoleOutputCP(Kernel32.CP_UTF8)) {
            throw Win32Error("Can't set std out CP to UTF8");
        }

        _configured = true;
    }

    public void Dispose() => Reset();

    public Task<uint> FlushAsync() {
        byte[] bytes = _writeBuffer.ToArray();
        if (!Kernel32.WriteConsole(_stdOut, bytes, (uint)bytes.Length, out uint count)) {
            throw new InvalidOperationException("Couldn't write to console");
        }

        _writeBuffer.Clear();
        return Task.FromResult(count);
    }

    public (int row, int col) GetCursorPos() {
        return Kernel32.GetConsoleScreenBufferInfo(_stdOut, out ConsoleScreenBufferInfo screenBufferInfo)
            ? (row: screenBufferInfo.CursorPosition.Y - 1, screenBufferInfo.CursorPosition.X - 1)
            : throw new InvalidOperationException("Can't get cursor position");
    }

    public (int rows, int cols) GetSize() {
        return Kernel32.GetConsoleScreenBufferInfo(_stdOut, out ConsoleScreenBufferInfo screenBufferInfo)
            ? (rows: screenBufferInfo.Size.Y, cols: screenBufferInfo.Size.X)
            : throw new InvalidOperationException("Can't get screen size");
    }

    public Task<int> ReadKeyAsync(CancellationToken token = default) => Task.FromResult(ReadKeyStageOne());

    public void Reset() {
        if (_configured) {
            Kernel32.SetConsoleMode(_stdIn, _oldStdInMode);
            Kernel32.SetConsoleMode(_stdOut, _oldStdOutMode);
        }
    }

    public void Write(byte[] bytes, int offset, int length) {
        byte[] copy = new byte[length];
        Array.Copy(bytes, offset, copy, 0, length);
        _writeBuffer.AddRange(copy);
    }

    private const char ESC = (char)0x1b;

    private const ConsoleInputMode StdInMode = ConsoleInputMode.EnableVirtualTemrinalInput;

    private const ConsoleOutputMode StdOutMode = ConsoleOutputMode.EnableProcessedOutput | ConsoleOutputMode.EnableVirtualTerminalProcessing | ConsoleOutputMode.DisableNewlineAutoreturn;

    private readonly bool _configured;

    private readonly uint _oldStdInMode;

    private readonly uint _oldStdOutMode;

    private readonly IntPtr _stdIn;

    private readonly IntPtr _stdOut;

    private readonly List<byte> _writeBuffer = new();

    private static string GetWin32Error(int win32ErrorCode) {
        StringBuilder result = new(1024);

        if (Kernel32.FormatMessage(
            FormatMessageFlags.FromSystem | FormatMessageFlags.IgnoreInserts,
            IntPtr.Zero,
            win32ErrorCode, 0,
            result,
            result.Capacity, Array.Empty<IntPtr>()) != 0) {
            return result.ToString();
        } else {
            return $"Unknown windows error '{win32ErrorCode:X}'";
        }
    }

    private static bool IsDigit(char c) => 'c' >= '0' && c <= '9';

    private static Exception Win32Error(string message) => new InvalidOperationException($"{message}: {GetWin32Error(Marshal.GetLastPInvokeError())}");

    private int ReadKeyStageOne() {
        char c = ReadKeyStageTwo();

        if (c == ESC) {
            // Could be the start of an escape sequecen
            char[] seq = new char[3];
            // Read the next couple of characters
            for (int i = 0; i < 2; i += 1) {
                seq[i] = ReadKeyStageTwo();
                if (seq[i] == 0) {
                    return ESC;
                }
            }

            // Yup, looks like an escape sequence
            if (seq[0] == '[') {
                if (IsDigit(seq[1])) {
                    seq[2] = ReadKeyStageTwo();
                    if (seq[2] == 0) {
                        return ESC;
                    }

                    if (seq[2] == '~') {
                        switch (seq[1]) {
                            case '1': return (int)KeyCodes.Home;
                            case '3': return (int)KeyCodes.Delete;
                            case '4': return (int)KeyCodes.End;
                            case '5': return (int)KeyCodes.PageUp;
                            case '6': return (int)KeyCodes.PageDown;
                            case '7': return (int)KeyCodes.Home;
                            case '8': return (int)KeyCodes.End;
                        }
                    }
                } else {
                    switch (seq[1]) {
                        case 'A': return (int)KeyCodes.ArrowUp;
                        case 'B': return (int)KeyCodes.ArrowDown;
                        case 'C': return (int)KeyCodes.ArrowRight;
                        case 'D': return (int)KeyCodes.ArrowLeft;
                        case 'H': return (int)KeyCodes.Home;
                        case 'F': return (int)KeyCodes.End;
                    }
                }
            } else if (seq[0] == 'O') {
                switch (seq[1]) {
                    case 'H': return (int)KeyCodes.Home;
                    case 'F': return (int)KeyCodes.End;
                }
            }

            return ESC;
        } else {
            return c;
        }
    }

    private char ReadKeyStageTwo() {
        while (true) {
            // Wait for activity on stdin
            WaitResult w = Kernel32.WaitForSingleObject(_stdIn, 100);
            switch (w) {
                case WaitResult.Signaled:
                    // Yay! Got activity Pull the activity
                    if (!Kernel32.ReadConsoleInput(_stdIn, out InputEvent inputEvent, 1, out uint _)) {
                        throw new InvalidOperationException("Can't read from console");
                    }

                    // If it's a key down event
                    if (inputEvent.EventType == InputEventType.KeyEvent && inputEvent.KeyEvent.KeyDown) {
                        // return it
                        return inputEvent.KeyEvent.Char;
                    }

                    break;

                case WaitResult.Timeout:
                    // Got bored of waiting
                    return '\0';

                default:
                    // Something went wrong
                    throw new InvalidOperationException("Unexpected result waiting for keyboard input");
            }
        }
    }
}

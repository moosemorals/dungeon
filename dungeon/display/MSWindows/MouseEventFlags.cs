﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

[Flags]
public enum MouseEventFlags {

    MouseMoved = 0x1,

    DoubleClick = 0x2,

    MouseWheelVirtical = 0x4,

    MouseWHeelHorizontal = 0x8
}

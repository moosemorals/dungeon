﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

public enum InputEventType : ushort {

    KeyEvent = 0x01,

    MouseEvent = 0x02,

    WindowBufferSizeEvent = 0x04,

    MenuEvent = 0x08,

    FocusEvent = 0x10,
}

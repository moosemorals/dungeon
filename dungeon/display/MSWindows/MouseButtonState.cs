﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

[Flags]
public enum MouseButtonState {

    LeftButton1 = 0x1,

    RightButton1 = 0x2,

    LeftButton2 = 0x4,

    LeftButton3 = 0x8,

    LeftButton4 = 0x10
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

using System.Runtime.InteropServices;

[StructLayout(LayoutKind.Sequential)]
public struct ConsoleScreenBufferInfo {

    public ConsoleScreenBufferInfo(Coord size, Coord cursorPosition, byte attributes, SmallRect window, Coord maximumWindowSize) {
        Size = size;
        CursorPosition = cursorPosition;
        Attributes = attributes;
        Window = window;
        MaximumWindowSize = maximumWindowSize;
    }

    public Coord Size;

    public Coord CursorPosition;

    public byte Attributes;

    public SmallRect Window;

    public Coord MaximumWindowSize;
}

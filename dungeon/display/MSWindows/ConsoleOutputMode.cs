﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display.MSWindows;

[Flags]
public enum ConsoleOutputMode : uint {

    /// <summary>
    /// Ouput characters are checked for VT100 sequqnces
    /// </summary>
    EnableProcessedOutput = 0x0001,

    /// <summary>
    /// Auto-wrap at end of line
    /// </summary>
    EnableWrapAtEOL = 0x0002,

    /// <summary>
    /// Enable VT100 sequence processing
    /// </summary>
    EnableVirtualTerminalProcessing = 0x0004,

    DisableNewlineAutoreturn = 0x0008,

    EnableLVBGridWorldwide = 0x0010
}

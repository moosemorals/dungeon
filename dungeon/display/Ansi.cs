﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.display;

using System;
using System.Linq;
using System.Text;

internal static class Ansi {

    /// <summary>
    /// Clear the whole screen
    /// </summary>
    public static byte[] ClearDisplay() => Command('J', 2);

    /// <summary>
    /// Clear from the top/left to the current cursor position
    /// </summary>
    public static byte[] ClearFromStartOfDisplay() => Command('J', 1);

    /// <summary>
    /// Clear the current line from col 1 to current cursor position
    /// </summary>
    public static byte[] ClearFromStartOfLine() => Command('K', 1);

    /// <summary>
    /// Clear the current line
    /// </summary>
    public static byte[] ClearLine() => Command('K', 2);

    /// <summary>
    /// Clear screen from cursor position to bottom/right
    /// </summary>
    public static byte[] ClearToEndOfDisplay() => Command('J');

    /// <summary>
    /// Clear the current line from the cursor position to the end of the line
    /// </summary>
    public static byte[] ClearToEndOfLine() => Command('K', 0);

    /// <summary>
    /// Move the cursor down n rows
    /// </summary>
    public static byte[] CursorDown(int n = 1) => Command('B', n);

    /// <summary>
    /// Move the cursor to the start of the line n rows down
    /// </summary>
    public static byte[] CursorDownLine(int n = 0) => Command('F', n);

    /// <summary>
    /// Move the cursor left n columns
    /// </summary>
    public static byte[] CursorLeft(int n = 1) => Command('D', n);

    /// <summary>
    /// Move the cursor right n columns
    /// </summary>
    public static byte[] CursorRight(int n = 1) => Command('C', n);

    /// <summary>
    /// Move the cursor to column c on the current row
    /// </summary>
    public static byte[] CursorSetCol(int c = 0) => Command('G', c);

    /// <summary>
    /// Move the cursor up n rows
    /// </summary>
    public static byte[] CursorUp(int n = 1) => Command('A', n);

    /// <summary>
    /// Move the cursor to the start of the row n rows up
    /// </summary>
    public static byte[] CursorUpLine(int n = 0) => Command('E', n);

    /// <summary>
    /// Delete n chars at the current cursor position, shifting text left
    /// </summary>
    public static byte[] DeleteChar(int n = 1) => Command('P', n);

    /// <summary>
    /// Delete n lines
    /// </summary>
    public static byte[] DeleteLines(int n = 1) => Command('M', n);

    /// <summary>
    /// Ask terminal to write cursor position to stdin as "\x1d[r;cR", where r is cursor
    /// row and c is cursor col
    /// </summary>
    /// <returns></returns>
    public static byte[] GetCursorPosition() => Command('n', 6);

    /// <summary>
    /// Ask the termial to write device attributes to stdin.
    /// </summary>
    /// <returns></returns>
    public static byte[] GetDeviceAttributes() => Command('c', '0');

    /// <summary>
    /// Hide the cursor
    /// </summary>
    public static byte[] HideCursor() => Command('l', '?', 25);

    /// <summary>
    /// Insert n lines above cursor (i.e., the line with the cursor moves down)
    /// </summary>
    public static byte[] InsertLines(int n = 1) => Command('L', n);

    /// <summary>
    /// Insert n spaces at the current cursor position, shifting text to the right.
    /// </summary>
    public static byte[] InsertSpace(int n = 1) => Command('@', n);

    public static byte[] MoveCursorHome() => Command('H');

    /// <summary>
    /// Replace n chars with spaces, starting at the current cursor position
    /// </summary>
    public static byte[] OverwriteChars(int n = 1) => Command('X', n);

    /// <summary>
    /// Reset color, bold, underline, inverse to defaults
    /// </summary>
    public static byte[] ResetColor() => Command('m', (int)AnsiColor.Reset);

    /// <summary>
    /// Move text down by n lines
    /// </summary>
    public static byte[] ScrollDown(int n = 1) => Command('T', n);

    /// <summary>
    /// Move text up by n lines
    /// </summary>
    public static byte[] ScrollUp(int n = 1) => Command('S', n);

    /// <summary>
    /// Set color, bold, underline, and/or inverse
    /// </summary>
    public static byte[] SetColor(params AnsiColor[] colors)
        => Command('m', colors.Cast<int>().ToArray());

    /// <summary>
    /// Move the cursor to the given row/col
    /// </summary>
    public static byte[] SetCursorPos(int c = 0, int r = 0) => Command('H', r + 1, c + 1);

    /// <summary>
    /// Set the shape/blink of the cursor
    /// </summary>
    public static byte[] SetCursorShape(AnsiCursor shape)
        => Command('q', ' ', (int)shape);

    /// <summary>
    /// Set the rows that are effected by scroll comamnds.
    /// </summary>
    public static byte[] SetScrollMargins(int top, int bottom) => Command('r', top, bottom);

    public static byte[] SetWindowTitle(string title) {
        byte[] titleBytes = Encoding.UTF8.GetBytes(title);

        byte[] command = new byte[SetTitlePrefix.Length + titleBytes.Length + SetTitleSuffix.Length];

        Array.Copy(SetTitlePrefix, command, SetTitlePrefix.Length);
        Array.Copy(titleBytes, 0, command, SetTitlePrefix.Length, titleBytes.Length);
        Array.Copy(SetTitleSuffix, 0, command, SetTitlePrefix.Length + titleBytes.Length, SetTitleSuffix.Length);
        return command;
    }

    /// <summary>
    /// Show the cursor
    /// </summary>
    public static byte[] ShowCursor() => Command('h', '?', 25);

    private static readonly byte[] Prefix = new byte[] { 0x1b, 0x5b }; // ESC[

    private static readonly byte[] SetTitlePrefix = new byte[] { 0x1b, 0x5d, 0x30, 0x3b }; // ESC]0;

    private static readonly byte[] SetTitleSuffix = new byte[] { 0x1b, 0x5c }; // ESC\

    private static byte[] Command(char name, char inter, int arg) {
        string cmd = $"{inter}{arg}{name}";
        byte[] bytes = new byte[Prefix.Length + 1 + cmd.Length];
        Array.Copy(Prefix, bytes, Prefix.Length);
        Array.Copy(Encoding.UTF8.GetBytes(cmd), 0, bytes, Prefix.Length, cmd.Length);
        return bytes;
    }

    private static byte[] Command(char name, params int[] args) {
        string cmd = $"{string.Join(';', args.Select(a => a.ToString()))}{name}";
        int cmdLen = Encoding.UTF8.GetByteCount(cmd);
        byte[] bytes = new byte[Prefix.Length + cmdLen];
        Array.Copy(Prefix, bytes, Prefix.Length);
        Array.Copy(Encoding.UTF8.GetBytes(cmd), 0, bytes, Prefix.Length, cmdLen);
        return bytes;
    }
}

internal enum AnsiCursor {

    BlinkingBar,

    BlinkingBlock,

    BlinkingUnderline,

    SteadyBar,

    SteadyBlock,

    SteadyUnderline,
}

public enum AnsiColor {

    Reset = 0,

    BoldOn = 1,

    BoldOff = 22,

    UnderlineOn = 4,

    UnderlineOff = 24,

    NegativeOn = 7,

    NegativeOff = 27,

    ForegroundBlack = 30,

    ForegroundRed = 31,

    ForegroundGreen = 32,

    ForegroundYellow = 33,

    ForegroundBlue = 34,

    ForegroundMagenta = 35,

    ForegroundCyan = 36,

    ForegroundWhite = 37,

    BackgroundBlack = 40,

    BackgroundRed = 41,

    BackgroundGreen = 42,

    BackgroundYellow = 43,

    BackgroundBlue = 44,

    BackgroundMagenta = 45,

    BackgroundCyan = 46,

    BackgroundWhite = 47,

    BrightForegroundBlack = 90,

    BrightForegroundRed = 91,

    BrightForegroundGreen = 92,

    BrightForegroundYellow = 93,

    BrightForegroundBlue = 94,

    BrightForegroundMagenta = 95,

    BrightForegroundCyan = 96,

    BrightForegroundWhite = 97,

    BrightBackgroundBlack = 100,

    BrightBackgroundRed = 101,

    BrightBackgroundGreen = 102,

    BrightBackgroundYellow = 103,

    BrightBackgroundBlue = 104,

    BrightBackgroundMagenta = 105,

    BrightBackgroundCyan = 106,

    BrightBackgroundWhite = 107,
}

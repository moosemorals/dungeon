﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

using System;

internal static class Grammar {

    internal static string MakeSecondPerson(string input) {
        string[] parts = input.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);

        parts[0] += 's';
        return string.Join(' ', parts);
    }

    internal static string Plural(string noun, int count) => count == 1 ? noun : $"{noun}s";
}

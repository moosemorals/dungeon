﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

using System.ComponentModel.DataAnnotations;

public enum KeyCodes {

    [Display(Name = "Arrow Down")]
    ArrowDown = 1001,

    [Display(Name = "Arrow Left")]
    ArrowLeft = 1002,

    [Display(Name = "Arrow Right")]
    ArrowRight = 1003,

    [Display(Name = "Arrow Up")]
    ArrowUp = 1000,

    [Display(Name = "Delete")]
    Delete = 1008,

    [Display(Name = "End")]
    End = 1007,

    [Display(Name = "Home")]
    Home = 1006,

    [Display(Name = "Page Down")]
    PageDown = 1005,

    [Display(Name = "Page Up")]
    PageUp = 1004,
}

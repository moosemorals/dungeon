﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

internal class Scheduler<T> {

    internal bool HasNext => _events.HasEntries;

    internal void Cancel(T item) => _events.Remove(item);

    internal T GetNext() {
        (T item, double priority) = _events.Dequeue();
        _events.AdjustPriorities(-priority);
        return item;
    }

    internal void Schedule(T item, double priority)
        => _events.Enqueue(item, priority);

    private readonly PriorityList<T> _events = new();
}

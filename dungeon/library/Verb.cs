﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;
using System;
using System.Linq;

internal record Verb {

    internal static readonly Verb Apply = new("feel", "looks");
    public string First { get; private init; }
    public string Second { get; private init; }

    public Verb(string first, string? second = null) {
        First = first;

        if (second == null) {
            string[] parts = first.Split(' ', 2, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            Second = $"{parts[0]}s {string.Join(' ', parts.Skip(1))}";
        } else {
            Second = second;
        } 
    }
}

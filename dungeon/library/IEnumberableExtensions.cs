﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

internal static class IListExtensions {

    public static void Shuffle<T>(this IList<T> list) {
        int n = list.Count;
        while (n-- > 1) {
            int k = Random.Shared.Next(n + 1);
            (list[n], list[k]) = (list[k], list[n]);
        }
    }
}

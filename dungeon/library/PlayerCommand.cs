﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

/// <summary>
/// A command from a player
/// </summary>
internal record PlayerCommand(string Name, Action Action, params int[] Keys);

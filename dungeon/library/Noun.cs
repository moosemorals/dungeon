﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

internal record Noun(string Word, int Count = 1, bool IsPlayer = false) {

    private const string Vowel = "aeiouAEIOU";

    internal Noun AsPlayer => this with { IsPlayer = true };
    internal Noun WithCount(int count) => this with { Count = count };

    public override string ToString() {
        if (Word.Length == 0) {
            return Word;
        }

        if (Count == 0) {
            return $"no {Word}s";
        } else if (Count > 1) {
            return $"{Count} {Word}s";
        } else if (IsPlayer) {
            return $"the {Word}";
        } else if (Vowel.Contains(Word[0])) {
            return $"an {Word}";
        } else {
            return $"a {Word}";
        }
    }


    public static readonly Noun You = new YouNoun();

    private record YouNoun : Noun {
        public YouNoun() : base("you", 1, true) { }

        public override string ToString() => "you";
    }
}

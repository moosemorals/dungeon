﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

using System;
using System.Collections.Generic;

internal class PriorityList<T> {

    public bool HasEntries => _queue.Any();

    public void AdjustPriorities(double delta) {
        foreach (PriorityEntry pe in _queue) {
            pe.Priority += delta;
        }
    }

    public (T, double) Dequeue() {
        PriorityEntry pe = _queue.First();
        _queue.RemoveFirst();
        return (pe.Item, pe.Priority);
    }

    public void Enqueue(T item, double priority = 0) {
        // Insert new entry into list after entry with lower or matching prioirity
        PriorityEntry newEntry = new(item, priority);
        LinkedListNode<PriorityEntry>? node = _queue.First;

        while (node != null && node.Value.Priority < priority) {
            node = node.Next;
        }

        if (node != null) {
            _queue.AddAfter(node, newEntry);
        } else {
            _queue.AddLast(newEntry);
        }
    }

    public void Remove(T item) {
        LinkedListNode<PriorityEntry>? node = _queue.First;
        while (node != null && !node.Value.Equals(item)) {
            node = node.Next;
        }

        if (node is not null) {
            _queue.Remove(node);
        }
    }

    private readonly LinkedList<PriorityEntry> _queue = new();

    private class PriorityEntry : IComparable<PriorityEntry> {

        public int CompareTo(PriorityEntry? other) {
            if (other is null) {
                return -1;
            } else {
                return Priority.CompareTo(other.Priority);
            }
        }

        internal PriorityEntry(T item, double priority) {
            Item = item;
            Priority = priority;
        }

        internal T Item { get; private init; }

        internal double Priority { get; set; }
    }
}

﻿// SPDX-Licence-Idenitfier: ISC

namespace uk.osric.dungeon.library;

using uk.osric.dungeon.actors;

internal interface IDrawable {

    char Glyph { get; }

    Noun Name { get; }

    Style Style { get; }
}
